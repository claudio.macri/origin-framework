<?php

namespace Origin;

class Container
{
    protected $bindings = [];
    protected $instances = [];

    public function instance($abstract, $instance)
    {
        // Salva l'istanza anche usando il nome della classe
        $this->instances[$abstract] = $instance;
        if (is_object($instance)) {
            $this->instances[get_class($instance)] = $instance;
        }
    }

    public function singleton($abstract, $concrete = null)
    {
        $this->bindings[$abstract] = [
            'concrete' => $concrete ?? $abstract,
            'shared' => true
        ];
    }

    public function make($abstract, array $parameters = [])
    {
        // Se esiste già un'istanza, restituiscila
        if (isset($this->instances[$abstract])) {
            return $this->instances[$abstract];
        }

        $concrete = $this->bindings[$abstract]['concrete'] ?? $abstract;

        if ($concrete instanceof \Closure) {
            $object = $concrete($this, $parameters);
        } else {
            // Usa i parametri forniti e risolvi solo le dipendenze mancanti
            $dependencies = $this->resolveDependencies($concrete, $parameters);
            $object = new $concrete(...$dependencies);
        }

        if (isset($this->bindings[$abstract]['shared']) && $this->bindings[$abstract]['shared']) {
            $this->instances[$abstract] = $object;
        }

        return $object;
    }

    protected function resolveDependencies($concrete, array $parameters = [])
    {
        $reflector = new \ReflectionClass($concrete);

        if (!$reflector->isInstantiable()) {
            throw new \Exception("Class {$concrete} is not instantiable");
        }

        $constructor = $reflector->getConstructor();

        if (is_null($constructor)) {
            return [];
        }

        $dependencies = [];

        foreach ($constructor->getParameters() as $parameter) {
            // Se il parametro è stato fornito, usalo
            if (array_key_exists($parameter->getName(), $parameters)) {
                $dependencies[] = $parameters[$parameter->getName()];
                continue;
            }

            $type = $parameter->getType();

            if (!$type || $type->isBuiltin()) {
                if ($parameter->isDefaultValueAvailable()) {
                    $dependencies[] = $parameter->getDefaultValue();
                } else {
                    throw new \Exception("Cannot resolve parameter {$parameter->getName()} of {$concrete}. Please provide it explicitly.");
                }
            } else {
                $typeName = $type->getName();
                $dependencies[] = $this->make($typeName);
            }
        }

        return $dependencies;
    }
}
