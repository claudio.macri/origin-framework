<?php

namespace Origin;

use Origin\Auth\Auth;

class View
{
    private $layout;
    private $data = [];
    private $sections = [
        'page_css' => [],
        'meta_tags' => [],
        'page_js' => []
    ];
    private static $shared = [];

    // Singleton per gestire l'istanza
    private function __construct() {}

    // Factory method per creare una nuova istanza
    public static function create()
    {
        $instance = new self();
        $instance->layout = 'main';
        return $instance;
    }

    public static function make(string $view, array $args = [])
    {
        $instance = new self();
        $instance->data($args);
        return $instance->render($view);
    }

    // Per specificare un layout custom
    public static function layout(string $layout)
    {
        $instance = self::create();
        $instance->layout = $layout;
        return $instance;
    }

    // Metodo per salvare variabili
    public function with(string $key, $value)
    {
        $this->data[$key] = $value;
        return $this;
    }

    // Metodo per aggiungere contenuti alle sezioni
    public function section(string $name, $content)
    {
        // Se la sezione è per css, js o meta, aggiungiamo all'array
        if (in_array($name, ['page_css', 'page_js', 'meta_tags'])) {
            if (!is_array($content)) {
                $content = [$content];
            }
            $this->sections[$name] = array_merge($this->sections[$name], $content);
        } else {
            // Per altre sezioni, sostituiamo il contenuto
            $this->sections[$name] = $content;
        }
        return $this;
    }

    // Metodi per variabili specifiche
    public function title($title)
    {
        return $this->with('title', $title);
    }

    public function description($description)
    {
        return $this->with('description', $description);
    }

    // Metodi helper per sezioni specifiche
    public function css($paths)
    {
        return $this->section('page_css', $paths);
    }

    public function js($paths)
    {
        return $this->section('page_js', $paths);
    }

    public function meta($tags)
    {
        return $this->section('meta_tags', $tags);
    }

    public function data(array $data)
    {
        foreach ($data as $key => $value) {
            $this->data[$key] = $value;
        }
        return $this;
    }

    public static function share($key, $value)
    {
        if (!in_array($key, ["title", "description", "sections", "content"])) {
            self::$shared[$key] = $value;
        }
    }

    // Metodo per renderizzare il template
    public function render(string $view)
    {
        extract(self::$shared);
        extract($this->data);
        $sections = $this->sections;

        if (!empty($this->layout)) {
            ob_start();
            include Config::get('filesystem.disks.views') . "/layouts/{$this->layout}/layout.php";
            return ob_get_clean();
        }

        ob_start();
        include Config::get('filesystem.disks.views') . "/$view.php";
        return ob_get_clean();
    }
}
