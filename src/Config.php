<?php

namespace Origin;

class Config
{
    /**
     * All of the configuration items.
     *
     * @var array
     */
    protected static $items = [];

    /**
     * Load configuration from a PHP file
     *
     * @param string $path Path to the config file
     * @param string|null $key Optional key to store the config under
     * @return void
     */
    public static function load($path, $key = null)
    {
        if (!file_exists($path)) {
            throw new \Exception("Configuration file not found: {$path}");
        }

        $config = require $path;

        if ($key) {
            static::set($key, $config);
        } else {
            static::$items = array_merge(static::$items, $config);
        }
    }

    /**
     * Load all configuration files from a directory
     *
     * @param string $directory
     * @return void
     */
    public static function loadFromDirectory($directory)
    {
        if (!is_dir($directory)) {
            throw new \Exception("Configuration directory not found: {$directory}");
        }

        foreach (glob($directory . '/*.php') as $file) {
            $key = basename($file, '.php');
            static::load($file, $key);
        }
    }

    /**
     * Get a configuration value using "dot" notation.
     *
     * @param string $key
     * @param mixed $default
     * @return mixed
     */
    public static function get($key, $default = null)
    {
        $keys = explode('.', $key);
        $config = static::$items;

        foreach ($keys as $key) {
            if (!is_array($config) || !array_key_exists($key, $config)) {
                return $default;
            }
            $config = $config[$key];
        }

        return $config;
    }

    /**
     * Set a configuration value using "dot" notation.
     *
     * @param string $key
     * @param mixed $value
     * @return void
     */
    public static function set($key, $value)
    {
        $keys = explode('.', $key);
        $config = &static::$items;

        while (count($keys) > 1) {
            $key = array_shift($keys);

            if (!isset($config[$key]) || !is_array($config[$key])) {
                $config[$key] = [];
            }

            $config = &$config[$key];
        }

        $config[array_shift($keys)] = $value;
    }

    /**
     * Check if a configuration value exists
     *
     * @param string $key
     * @return bool
     */
    public static function has($key)
    {
        return static::get($key) !== null;
    }

    /**
     * Get all configuration items
     *
     * @return array
     */
    public static function all()
    {
        return static::$items;
    }

    /**
     * Clear all configuration items
     *
     * @return void
     */
    public static function clear()
    {
        static::$items = [];
    }
}
