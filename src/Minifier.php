<?php

namespace Origin;

use Exception;
use MatthiasMullie\Minify;

class Minifier
{
    static function minifyJS($filename, $output = null)
    {
        $dir_js = Config::get('filesystem.disks.root') . "/public/js";

        if (!file_exists($filename)) {
            throw new Exception("Il file sorgente non esiste: $filename");
        }

        // Ottieni il nome del file senza estensione
        $fileInfo = pathinfo($filename);
        $outputFileName = $fileInfo['filename'] . '.min.js';

        // Determina la cartella di output
        $output ??= $dir_js;

        // Crea la cartella di output se non esiste
        if (!file_exists($output)) {
            mkdir($output, 0777, true);
        }

        // Percorsi completi per il file sorgente e il file minificato
        $sourcePath = $filename;
        $outputPath = rtrim($output, '/') . '/' . $outputFileName;

        if (!file_exists($outputPath) || filemtime($outputPath) < filemtime($sourcePath)) {
            // Esegui la minificazione e la copia solo se necessario
            $minifier = new Minify\JS($sourcePath);
            $minifier->minify($outputPath);
        }

        return $outputPath; // Restituisci il percorso del file minificato        
    }

    static function minifyCSS($filename, $output = null)
    {
        $dir_css = Config::get('filesystem.disks.root') . "/public/css";

        if (!file_exists($filename)) {
            throw new Exception("Il file sorgente non esiste: $filename");
        }

        // Ottieni il nome del file senza estensione
        $fileInfo = pathinfo($filename);
        $outputFileName = $fileInfo['filename'] . '.min.css';

        // Determina la cartella di output
        $output ??= $dir_css;

        // Crea la cartella di output se non esiste
        if (!file_exists($output)) {
            mkdir($output, 0777, true);
        }

        // Percorsi completi per il file sorgente e il file minificato
        $sourcePath = $filename;
        $outputPath = rtrim($output, '/') . '/' . $outputFileName;

        if (!file_exists($outputPath) || filemtime($outputPath) < filemtime($sourcePath)) {
            // Esegui la minificazione e la copia solo se necessario
            $minifier = new Minify\CSS($sourcePath);
            $minifier->minify($outputPath);
        }

        return $outputPath; // Restituisci il percorso del file minificato 
    }

    static function minify($filename, $output)
    {
        $extension = pathinfo($filename, PATHINFO_EXTENSION);

        if ($extension == "js") {
            return self::minifyJS($filename, $output);
        }
        if ($extension == "css") {
            return self::minifyCSS($filename, $output);
        }

        return null;
    }

    static function minifyFilesRecursively($sourceDir, $outputDir)
    {
        $files = scandir($sourceDir);

        foreach ($files as $file) {
            if ($file !== '.' && $file !== '..') {
                $sourcePath = $sourceDir . '/' . $file;
                $outputPath = $outputDir . '/' . $file;

                if (is_dir($sourcePath)) {
                    // Se è una sottodirectory, ricorsivamente minifica i file al suo interno
                    if (!file_exists($outputPath)) {
                        mkdir($outputPath, 0777, true);
                    }
                    self::minifyFilesRecursively($sourcePath, $outputPath);
                } elseif (in_array(pathinfo($file, PATHINFO_EXTENSION), ["js", "css"])) {
                    try {
                        // echo $sourcePath . "<br>" . $outputPath;exit;
                        $minifiedPath = self::minify($sourcePath, $outputDir);
                    } catch (Exception $e) {
                        echo "Errore: " . $e->getMessage() . "<br>";
                    }
                }
            }
        }
    }
}
