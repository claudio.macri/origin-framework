<?php

namespace Origin\Auth;

use Origin\Config;
use Origin\Configuration;

class UserProvider
{
    protected $model;
    protected $tokenName;

    public function __construct()
    {
        $this->model = Config::get("auth.users.model");
        $this->tokenName = Config::get('api.token_name', 'api_token');
    }

    public function retrieveById($id)
    {
        return $this->newDAO()->getById($id);
    }

    public function retrieveByCredentials($credentials)
    {
        $model = $this->createModel();
        $query = $model->newBuilder();

        $results = $query->getWhere($credentials, "model", $this->model);

        return (!empty($results)) ? $results[0] : null;
    }

    public function retrieveByToken($token)
    {
        $credentials = [
            $this->tokenName => $token
        ];

        return $this->retrieveByCredentials($credentials);
    }

    public function getTokenName()
    {
        return $this->tokenName;
    }

    public function newDAO() {
        return $this->createModel()->newModelDAO();
    }
    public function createModel()
    {
        $class = '\\'.ltrim($this->model, '\\');

        return new $class;
    }
}
