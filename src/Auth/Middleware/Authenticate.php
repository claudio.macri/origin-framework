<?php

namespace Origin\Auth\Middleware;

use Origin\Auth\Auth;
use Origin\Http\Response;

class Authenticate
{
    /**
     * Handle the incoming request
     */
    public function handle($request, $guard = null)
    {
        $this->authenticate($request, $guard);
    }

    /**
     * Authenticate the request
     */
    protected function authenticate($request, $guard)
    {
        $auth = Auth::setAuth($request, $guard);

        if (!$auth->check()) {
            return $this->handleUnauthenticated($request);
        }
    }

    /**
     * Handle unauthenticated request based on request type
     */
    protected function handleUnauthenticated($request)
    {
        if ($request->ajax()) {
            return $this->respondToUnauthorizedAjax();
        }

        if ($request->is('api/*')) {
            return $this->respondToUnauthorizedApi();
        }

        return $this->redirectTo($request);
    }

    /**
     * Handle unauthorized AJAX request
     */
    protected function respondToUnauthorizedAjax()
    {
        return Response::json([
            'error' => 'Unauthenticated.',
            'code' => 401
        ], 401);
    }

    /**
     * Handle unauthorized API request
     */
    protected function respondToUnauthorizedApi()
    {
        return Response::json([
            'error' => 'Unauthorized',
            'code' => 401
        ], 401);
    }

    protected function redirectTo($request)
    {
        //
    }
}
