<?php

namespace Origin\Auth;

use Origin\Config;
use Origin\Http\Session;

class SessionAuth
{
    use AuthHelpers;

    protected $session;
    protected $sessionKey;
    protected $request;

    public function __construct()
    {
        $this->provider = new UserProvider;
        $this->session = new Session;

        $this->sessionKey = Config::get("auth.sess_key", "user_id");
    }

    public function user()
    {
        if ($this->loaded) {
            return $this->user;
        }

        $id = $this->session->get($this->sessionKey);

        if (!$id) {
            return null;
        }

        $this->user = $this->provider->retrieveById($id);
        $this->loaded = true;

        return $this->user;
    }

    public function validate(array $credentials)
    {
        $user = $this->provider->retrieveByCredentials($credentials);

        if ($user) {
            $this->login($user);
            return true;
        }

        return false;
    }

    public function login($user)
    {
        $this->session->set($this->sessionKey, $user->id());
        $this->setUser($user);

        return $this;
    }

    public function logout()
    {
        $this->session->remove($this->sessionKey);
        $this->forgetUser();

        return $this;
    }

    public function setRequest($request)
    {
        $this->request = $request;
        return $this;
    }
}
