<?php

namespace Origin\Auth;

use Origin\Config;
use Origin\Configuration;

class TokenAuth
{
    use AuthHelpers;

    protected $request;
    protected $tokenKey;

    public function __construct()
    {
        $this->provider = new UserProvider;
        $this->tokenKey = Config::get('api.token_name', 'api_token');
    }

    function user()
    {
        if ($this->loaded) {
            return $this->user;
        }

        $token = $this->getTokenFromRequest();

        if ($token) {
            $this->user = $this->provider->retrieveByToken($token);
            $this->loaded = true;
        }

        return $this->user;
    }

    public function validate(array $credentials)
    {
        $token = $credentials[$this->tokenKey] ?? null;

        if (!$token) {
            return false;
        }

        $user = $this->provider->retrieveByToken($token);

        if ($user) {
            $this->setUser($user);
            return true;
        }

        return false;
    }

    protected function getTokenFromRequest()
    {
        return $this->request->bearerToken() ?? $this->request->{$this->tokenKey} ?? null;
    }

    public function setRequest($request)
    {
        $this->request = $request;
        return $this;
    }
}
