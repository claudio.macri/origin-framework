<?php

namespace Origin\Auth;

use Origin\Logger\Log;

trait AuthHelpers
{
    protected $provider;
    protected $user;
    protected $loaded = false;

    public function check()
    {
        return !is_null($this->user());
    }


    public function id()
    {
        $user = $this->user();
        return $user ? ($user->id() ?? null) : null;
    }

    public function setUser($user)
    {
        $this->user = $user;
        $this->loaded = true;
        return $this;
    }

    public function getProvider()
    {
        return $this->provider;
    }

    public function forgetUser()
    {
        $this->user = null;
        $this->loaded = false;
    }
}
