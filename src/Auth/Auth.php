<?php

namespace Origin\Auth;

use Origin\Config;
use Origin\Http\Session;

class Auth
{
    protected static $auth = null;

    /**
     * Get auth instance
     */
    public static function getAuth()
    {
        if (!self::$auth) {
            self::$auth = self::resolveAuth();
        }

        return self::$auth;
    }

    /**
     * Resolve which auth driver to use
     */
    protected static function resolveAuth()
    {
        return Session::checkStatus()
            ? new SessionAuth()
            : new TokenAuth();
    }

    /**
     * Set auth driver explicitly
     */
    public static function setAuth($request, $guard = null)
    {
        $modes = Config::get('auth.modes');
        $defaultMode = Config::get('auth.default');

        $authClass = $modes[$guard ?? $defaultMode];

        $auth = new $authClass();
        $auth->setRequest($request);

        return self::$auth = $auth;
    }

    /**
     * Forward calls to auth instance
     */
    public static function __callStatic($method, $args)
    {
        $auth = self::getAuth();

        if (method_exists($auth, $method)) {
            return $auth->$method(...$args);
        }

        throw new \BadMethodCallException("Method [$method] does not exist.");
    }
}
