<?php

namespace Origin\Routing;

use Closure;
use Origin\Config;
use Origin\Configuration;
use Origin\Http\Controller;
use Origin\Http\Request;

class Router
{
    /**
     * RouteCollection instance
     */
    protected $routes;

    /**
     * Route corrente
     */
    protected $current;

    /**
     * Request corrente
     */
    protected $currentRequest;
    /**
     * Middleware globali
     */
    protected $middleware = [];

    /**
     * Gruppi di middleware
     */
    protected $middlewareGroups = [];

    protected $groupStack = [];

    /**
     * Instance del router (singleton)
     */
    protected static $router = null;

    public function __construct()
    {
        $this->routes = new RouteCollection;
    }

    public function get($uri, $action)
    {
        return $this->addRoute($uri, "GET", $action);
    }

    public function post($uri, $action)
    {
        return $this->addRoute($uri, "POST", $action);
    }

    public function put($uri, $action)
    {
        return $this->addRoute($uri, "PUT", $action);
    }

    public function delete($uri, $action)
    {
        return $this->addRoute($uri, "DELETE", $action);
    }

    public function addRoute($uri, $method, $action)
    {
        $route = new Route($uri, $method, $action);

        // Genera nome automaticamente
        $route->name = $this->generateRouteName($route);

        $this->routes->add($route);

        return $route;
    }

    public function group(array $attributes, $callback)
    {
        $this->groupStack[] = $attributes;

        if ($callback instanceof Closure) {
            $previousRouter = self::$router;
            self::$router = $this->getInsideInstance();

            $callback($this->currentRequest);
            $routes = self::$router->getRoutes()->all();

            self::$router = $previousRouter;

            foreach ($routes as $route) {
                $this->applyGroupAttributes($route, $attributes);
                $this->routes->add($route);
            }
        }

        array_pop($this->groupStack);
    }

    public function resource($name, $controller)
    {
        $resourceRoutes = [
            ['GET', "/$name", 'index', $name . '.index'],
            ['GET', "/$name/create", 'create', $name . '.create'],
            ['POST', "/$name", 'store', $name . '.store'],
            ['GET', "/$name/{id}", 'show', $name . '.show'],
            ['GET', "/$name/{id}/edit", 'edit', $name . '.edit'],
            ['PUT', "/$name/{id}", 'update', $name . '.update'],
            ['DELETE', "/$name/{id}", 'delete', $name . '.delete'],
        ];

        foreach ($resourceRoutes as [$method, $uri, $action, $routeName]) {
            $route = $this->addRoute($uri, $method, [$controller, $action]);
            // Applica il nome considerando i prefissi dei gruppi
            $route->name($this->buildRouteName($routeName));
        }
    }

    public function middleware($middleware, $callback)
    {
        return $this->group(['middleware' => $middleware], $callback);
    }

    public function prefix($prefix, $callback)
    {
        return $this->group(['prefix' => $prefix], $callback);
    }

    protected function applyGroupAttributes($route, $attributes)
    {
        if (isset($attributes['middleware'])) {
            $route->addMiddleware($attributes['middleware']);
        }

        if (isset($attributes['prefix'])) {
            $route->uri = "/" . trim($attributes['prefix'], "/") . "/" . ltrim($route->uri, "/");

            // Aggiorna il nome della route se presente
            if ($route->name) {
                $route->name = $this->buildRouteName($route->name);
            }
        }

        if (
            isset($attributes['namespace']) &&
            is_array($route->action) &&
            isset($route->action['controller'])
        ) {
            $route->action['controller'][0] = $attributes['namespace'] . '\\' . ltrim($route->action['controller'][0], '\\');
        }
    }

    /**
     * Crea le route API per un resource controller
     */
    public function apiResource($name, $controller)
    {
        $apiRoutes = [
            // GET /users - Lista risorse
            ['GET', "/$name", 'index'],

            // GET /users/{id} - Singola risorsa
            ['GET', "/$name/{id}", 'show'],

            // POST /users - Crea risorsa
            ['POST', "/$name", 'store'],

            // PUT /users/{id} - Aggiorna risorsa
            ['PUT', "/$name/{id}", 'update'],

            // DELETE /users/{id} - Elimina risorsa
            ['DELETE', "/$name/{id}", 'delete'],
        ];

        // Se il controller inizia con / considera già il namespace completo
        if (!str_starts_with($controller, '\\')) {
            $controller = 'App\\Http\\Controllers\\' . $controller;
        }

        foreach ($apiRoutes as [$method, $uri, $action]) {
            $this->addRoute($uri, $method, [$controller, $action]);
        }
    }

    /**
     * Crea le route API per più resource controller
     */
    public function apiResources(array $resources)
    {
        foreach ($resources as $name => $controller) {
            $this->apiResource($name, $controller);
        }
    }

    public function dispatch($request)
    {
        $this->currentRequest = $request;
        $route = $this->routes->match($request);
        $request->setRoute($route);
        $this->current = $route;

        if (is_null($route)) {
            return false;
        }

        return $route->run($request);
    }

    public function call($method, $uri, $query = [], $params = [], $files = [])
    {
        $request = Request::create(Config::get('app.base_url') . $uri, $method, $query, $params, $files);
        return $this->dispatch($request);
    }

    public static function getInstance()
    {
        if (is_null(self::$router)) {
            self::$router = new static;
        }
        return self::$router;
    }

    public function getInsideInstance()
    {
        $router = new static;
        $router->setMiddleware($this->getMiddleware());
        return $router;
    }

    public function route($name, $parameters = [], $query = [], $absolute = true)
    {
        $route = $this->routes->getByName($name);

        if ($route === null) {
            throw new \Exception("Route [{$name}] not defined.");
        }

        $uri = $route->uri;

        // Trova tutti i parametri richiesti nella route
        preg_match_all('/\{([^}]+)\}/', $uri, $requiredParams);
        $requiredParams = $requiredParams[1];

        // Verifica che tutti i parametri richiesti siano stati forniti
        foreach ($requiredParams as $param) {
            if (!isset($parameters[$param])) {
                throw new \Exception("Missing required parameter [{$param}] for route [{$name}].");
            }
        }

        // Sostituisce i parametri nell'URI
        foreach ($parameters as $key => $value) {
            $uri = str_replace("{{$key}}", $value, $uri);
            unset($parameters[$key]); // Rimuove i parametri usati
        }

        // Aggiunge i query parameters rimanenti
        if (!empty($query)) {
            $uri .= '?' . http_build_query($query);
        }

        // Aggiunge il dominio se richiesto
        if ($absolute) {
            $uri = rtrim(Config::get('app.base_url'), '/') . '/' . ltrim($uri, '/');
        }

        return $uri;
    }

    protected function generateRouteName($route)
    {
        // Se è già stato impostato un nome, usa quello con i prefissi dei gruppi
        if ($route->name) {
            return $this->buildRouteName($route->name);
        }

        $name = '';

        // Se c'è un controller
        if (isset($route->action['controller'])) {
            $controller = $route->action['controller'][0];
            $method = $route->action['controller'][1];

            // Prendi l'ultima parte del nome del controller (es: da App\Http\Controllers\UserController a user)
            $controllerName = strtolower(basename(str_replace('\\', '/', $controller)));
            $controllerName = str_replace('controller', '', $controllerName);

            // Costruisci il nome base
            $name = $controllerName . '.' . $method;
        } else {
            // Se è una closure, usa l'URI
            $name = str_replace(['/', '{', '}'], ['.', '', ''], trim($route->uri, '/'));
        }

        return $this->buildRouteName($name);
    }

    protected function buildRouteName($name)
    {
        $prefix = '';

        // Itera attraverso lo stack dei gruppi per costruire il prefisso
        foreach ($this->groupStack as $group) {
            if (isset($group['prefix']) && !str_starts_with($name, $group['prefix'])) {
                $prefix .= $group['prefix'] . '.';
            }
        }

        return $prefix . $name;
    }

    function getRoutes()
    {
        return $this->routes;
    }

    function setMiddleware($middleware)
    {
        $this->middleware = $middleware;
    }

    function getMiddleware()
    {
        return $this->middleware;
    }

    function getCurrentRoute()
    {
        return $this->current;
    }
}
