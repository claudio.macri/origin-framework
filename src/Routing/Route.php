<?php

namespace Origin\Routing;

use Exception;
use ReflectionClass;

/**
 * @method static Route get(string $uri, mixed $action)
 * @method static Route post(string $uri, mixed $action)
 * @method static Route put(string $uri, mixed $action)
 * @method static Route patch(string $uri, mixed $action)
 * @method static Route delete(string $uri, mixed $action)
 * @method static void group(array $attributes, callable $callback) 
 * @method static void middleware(string|array $middleware, callable $callback)
 * @method static void prefix(string $prefix, callable $callback)
 * @method static void resource(string $name, string $controller)
 * @method static string|null route(string $name, array $parameters = [], array $query = [], bool $absolute = true)
 */
class Route
{
    /**
     * URI della route
     */
    public $uri;

    /**
     * Metodo HTTP della route
     */
    public $method;

    /**
     * Action (controller o closure)
     */
    public $action;

    /**
     * Nome della route
     */
    public $name;

    /**
     * Controller associato
     */
    public $controller;

    /**
     * Middleware della route
     */
    public $middleware = [];

    /**
     * Parametri della route
     */
    protected $parameters = [];

    /**
     * Vincoli sui parametri
     */
    protected $wheres = [];

    /**
     * Pattern regex per i parametri
     */
    protected $patterns = [
        'id' => '[0-9]+',
        'slug' => '[a-z0-9-]+',
        'any' => '[^/]+',
        'num' => '[0-9]+',
        'alpha' => '[a-zA-Z]+',
        'alphanum' => '[a-zA-Z0-9]+',
    ];

    /**
     * Metodi HTTP supportati
     */
    public static $verbs = ['GET', 'POST', 'PUT', 'PATCH', 'DELETE'];

    /**
     * Gruppi di routes supportati
     */
    public static $groups = ['group', 'controller', 'resource', 'middleware', 'prefix'];

    /**
     * Crea una nuova istanza di Route
     */
    public function __construct($uri, $method, $action)
    {
        $this->uri = $uri;
        $this->method = strtoupper($method);
        $this->action = $this->parseAction($action);
    }

    /**
     * Verifica se la route corrisponde alla request
     */
    public function matches($request)
    {
        // Verifica il metodo HTTP
        if ($this->method !== $request->getMethod()) {
            return false;
        }

        $path = rtrim($request->getPathInfo(), '/') ?: '/';

        // Verifica il pattern dell'URI
        if (!preg_match($this->getRegex(), rawurldecode($path), $matches)) {
            return false;
        }

        // Estrai e salva i parametri
        $this->parameters = $this->extractParameters($matches);

        return true;
    }

    /**
     * Esegue la route e restituisce la response
     */
    public function run($request)
    {
        // Risolvi l'alias usando gli aliases dal Router
        $aliases = Router::getInstance()->getMiddleware();

        // Esegui middleware della route
        foreach (array_reverse($this->middleware) as $middleware) {
            if (is_array($middleware)) {
                $middlewareClass = $middleware["middleware"];
                $middlewareArgs = $middleware["args"];
            } else {
                // Se è un alias, usa la classe corrispondente
                $middlewareClass = $aliases[$middleware] ?? $middleware;
                $middlewareArgs = null;
            }

            call_user_func_array([new $middlewareClass, "handle"], [$request, $middlewareArgs]);
        }

        if (is_array($this->action) && isset($this->action["controller"])) {
            return $this->runControllerAction($request);
        }

        if ($this->action instanceof \Closure) {
            return ($this->action)($request);
        }

        return null;
    }

    /**
     * Esegue un controller
     */
    protected function runControllerAction($request)
    {
        [$controllerClass, $method] = $this->action["controller"];
        $controller = new $controllerClass;

        // Esegui middleware del controller
        if (method_exists($controller, 'getMiddleware')) {
            foreach ($controller->getMiddleware() as $middleware) {
                $middlewareClass = $middleware["middleware"];
                $middlewareArgs = $middleware["args"];
                call_user_func_array([new $middlewareClass, "handle"], [$request, $middlewareArgs]);
            }
        }

        // Se c'è un parametro 'action', sovrascrive il metodo
        if (isset($this->parameters['action'])) {
            $method = $this->parameters['action'];
        }

        // Reflection per dependency injection
        $controllerReflection = new ReflectionClass($controllerClass);
        $methodReflection = $controllerReflection->getMethod($method);
        $parameters = $methodReflection->getParameters();

        // Prepara gli argomenti
        $args = $this->resolveMethodParameters($parameters, $request);

        return call_user_func_array([$controller, $method], $args);
    }

    /**
     * Risolve le dipendenze di un metodo
     */
    protected function resolveMethodParameters($parameters, $request)
    {
        $args = [];
        foreach ($parameters as $param) {
            $paramName = $param->getName();
            if ($paramName === 'request') {
                $args[] = $request;
            } elseif (isset($this->parameters[$paramName])) {
                $args[] = $this->parameters[$paramName];
            } elseif ($param->isOptional()) {
                $args[] = $param->getDefaultValue();
            } else {
                throw new \InvalidArgumentException("Missing required parameter: {$paramName}");
            }
        }
        return $args;
    }

    /**
     * Aggiunge un nome alla route
     */
    public function name($name)
    {
        $this->name = $name;
    }

    /**
     * Aggiunge middleware alla route
     */
    public function addMiddleware($middleware)
    {
        if (is_string($middleware)) {
            $middleware = func_get_args();
        }

        foreach ($middleware as $mid) {
            $this->middleware[] = $mid;
        }

        return $this;
    }

    /**
     * Aggiunge un vincolo a un parametro
     */
    public function where($name, $pattern = null)
    {
        if (is_array($name)) {
            foreach ($name as $key => $pattern) {
                $this->where($key, $pattern);
            }
            return $this;
        }

        $this->wheres[$name] = $pattern;
        return $this;
    }

    /**
     * Genera il pattern regex per la route
     */
    public function getRegex()
    {
        /* // Escape della stringa URI e sostituzione delle parentesi graffe con i pattern regex appropriati
        $route = preg_quote($this->uri, '/');

        // Sostituisci "{action}" con un pattern regex che accetta un nome di metodo valido
        $route = preg_replace('/\\\{action\\\}/', '([a-zA-Z_\x7f-\xff][a-zA-Z0-9_\x7f-\xff]*)', $route);

        // Sostituisci "{id}" con un pattern regex che accetta solo numeri
        $route = preg_replace('/\\\{id\\\}/', '(\d+)', $route);

        // Sostituisci qualsiasi altro placeholder {nome_placeholder} con un pattern regex che accetta stringhe alfanumeriche
        // escludendo esplicitamente 'id' e 'action' come nomi di placeholder
        $route = preg_replace('/\\\{(?!id\\\}|action\\\})([a-zA-Z_][a-zA-Z0-9_]*)\\\}/', '(?P<$1>[a-zA-Z0-9_]+)', $route);

        // Aggiungi delimitatori regex
        $regex = '#^' . $route . '$#';

        return $regex; */

        $uri = $this->uri;

        // Sostituisci i parametri con i pattern regex
        foreach ($this->wheres as $name => $pattern) {
            $uri = str_replace("{{$name}}", "({$pattern})", $uri);
        }

        // Sostituisci i parametri rimanenti con il pattern di default
        $uri = preg_replace('/\{([^}]+)\}/', '([^/]+)', $uri);

        return '#^' . $uri . '$#';
    }

    /**
     * Estrae i parametri dai matches regex
     */
    protected function extractParameters($matches)
    {
        array_shift($matches); // Rimuovi il match completo

        $parameters = [];
        preg_match_all('/\{([^}]+)\}/', $this->uri, $paramNames);

        foreach ($paramNames[1] as $index => $name) {
            $parameters[$name] = $matches[$index] ?? null;
        }

        return $parameters;
    }

    /**
     * Parsing dell'action
     */
    protected function parseAction($action)
    {
        if (is_array($action) && isset($action[0], $action[1])) {
            return [
                'controller' => [$action[0], $action[1]]
            ];
        }
        return $action;
    }

    public static function __callStatic($method, $arguments)
    {
        $router = Router::getInstance();

        if (in_array(strtoupper($method), self::$verbs)) {
            return $router->$method(...$arguments);
        }
        if (in_array($method, self::$groups)) {
            return $router->$method(...$arguments);
        }
        if (method_exists($router, $method)) {
            return $router->$method(...$arguments);
        }

        throw new Exception("Method [$method] not found.");
    }
}
