<?php

namespace Origin\Routing;

class RouteCollection
{
    public $routes = [];
    public $nameList = [];
    /**
     * Cache delle routes per performance
     */
    protected $routesByMethod = [];

    public function add($route)
    {
        $this->routes[] = $route;

        // Aggiungi alla cache per metodo
        $this->routesByMethod[$route->method][] = $route;

        if ($route->name) {
            $this->nameList[$route->name] = $route;
        }
    }

    public function match($request)
    {
        $routes = $this->get($request->getMethod());

        foreach ($routes as $route) {
            if ($route->matches($request)) {
                return $route;
            }
        }

        return null;
    }

    public function all()
    {
        return $this->routes;
    }

    public function get($method)
    {
        // Usa la cache invece di iterare ogni volta
        if (isset($this->routesByMethod[$method])) {
            return $this->routesByMethod[$method];
        }
        return [];
    }

    public function getByName($name)
    {
        return array_key_exists($name, $this->nameList) ? $this->nameList[$name] : null;
    }

    public function getByAction($action) {}
}
