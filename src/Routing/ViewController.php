<?php 

namespace Origin\Routing;
use Origin\Http\Controller;
use Origin\View;

class ViewController extends Controller
{
    function __invoke($args)
    {
        return View::make($args["view"], $args["data"]);
    }
}