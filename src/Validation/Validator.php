<?php

namespace Origin\Validation;

use DateTime;
use Exception;
use Origin\File\UploadedFile;
use ReflectionClass;
use ReflectionMethod;

class Validator
{
    protected $isValid;
    protected $data = [];
    protected $rules = [];
    protected $messages = [];
    protected $errors = [];
    protected $customRules = [];
    protected $customMessages = [];
    protected $filesRules = [
        "file",
        "maxsize",
        "minsize",
        "mimes",
        "mimetypes"
    ];
    protected $defaultRules = [
        "required",
        "nullable",
        "email",
        "date",
        "number",
        "boolean",
        "array"
    ];

    protected $defaultMessages = [
        "required" => "Il campo :attribute è obbligatorio",
        "email" => "Indirizzo email non valido",
        "date" => "Data non valida",
        "number" => "Il valore dovrebbe essere un numero",
        "boolean" => "Il campo :attribute deve essere booleano",
        "array" => "Il campo :attribute deve essere un array"
    ];

    protected $filesMessages = [
        "file" => "File :attribute non valido",
        "maxsize" => "Il file :attribute ha una dimensione maggiore della dimensione massima consentita",
        "minsize" => "Il file :attribute ha una dimensione minore della dimensione minima consentita",
        "mimes" => "Il file :attribute non ha un'estensione consentita",
        "mimetypes" => "La tipologia del file :attribute non è consentita"
    ];

    public static function make(array $data, array $rules, array $messages = [])
    {
        $validator = new static($data, $rules, $messages);
        return $validator;
    }

    public function __construct(array $data, array $rules, array $messages = [])
    {
        $this->data = $data;
        $this->rules = $rules;
        $this->messages = $messages;
    }

    public function passes()
    {
        foreach ($this->rules as $attribute => $rules) {
            $value = $this->data[$attribute] ?? null;

            if ($rules[0] == "nullable" && (empty($value))) {
                $rules = [$rules[0]];
            }

            foreach ($rules as $rule) {

                list($rule, $param) = strpos($rule, ":") !== false ? explode(":", $rule, 2) : [$rule, null];

                $params = !is_null($param) ? explode(",", $param) : [];


                foreach ($params as $k => $param) {
                    if (array_key_exists($param, $this->rules)) {
                        $params[$k] = $this->data[$param] ?? null;
                    }
                }

                $validatorMethod = $this->getValidator($rule);

                if (isset($validatorMethod) && !call_user_func([$this, $validatorMethod], $value, ...$params)) {
                    $this->addError($attribute, $rule, $param);
                    $this->isValid = false;
                    return false;
                }
            }
        }

        $this->isValid = true;
        return true;
    }

    public function fails()
    {
        return !$this->passes();
    }

    public function validate()
    {
        if (isset($this->isValid) && $this->fails()) {
            return false;
        }

        if (!$this->isValid) {
            return false;
        }

        $results = [];

        foreach ($this->rules as $attribute => $rules) {
            $results[$attribute] = $this->data[$attribute] ?? null;
        }

        return $results;
    }

    public function validated()
    {
        return $this->validate();
    }

    public function errors()
    {
        return $this->errors;
    }

    public function addError($attribute, $rule, $param)
    {
        if (array_key_exists($attribute, $this->messages)) {
            $message = $this->messages[$attribute][$rule];
        } else if (array_key_exists($rule, $this->customMessages)) {
            $message = $this->customMessages[$rule];
        } else if (array_key_exists($rule, $this->defaultMessages)) {
            $message = $this->defaultMessages[$rule];
        } else if (array_key_exists($rule, $this->filesMessages)) {
            $message = $this->filesMessages[$rule];
        }

        $message = strtr($message, [":attribute" => $attribute]);
        $this->errors[$attribute] = $message;
    }

    public function validateRequired($value)
    {
        return isset($value);
    }

    public function validateNullable($value)
    {
        return true;
    }

    public function validateEmail($value)
    {
        return filter_var($value, FILTER_VALIDATE_EMAIL) !== false;
    }

    public function validateDate($value, $format = null)
    {
        $format ??= "Y-m-d";

        $d = DateTime::createFromFormat($format, $value);
        return $d && $d->format($format) === $value;
    }

    public function validateNumber($value)
    {
        return (filter_var($value, FILTER_VALIDATE_INT) || filter_var($value, FILTER_VALIDATE_FLOAT)) !== false;
    }

    public function validateBoolean($value)
    {
        if (!isset($value)) {
            return true;
        }
        return filter_var($value, FILTER_VALIDATE_BOOLEAN) !== false;
    }

    public function validateArray($value)
    {
        return is_array($value);
    }

    public function validateFile($file)
    {
        return $file instanceof UploadedFile && $file->isValid();
    }

    public function validateMaxSize($file, $max)
    {
        $size = ($file->getSize() / 1024) / 1024;

        return $max > $size;
    }

    public function validateMinSize($file, $min)
    {
        $size = ($file->getSize() / 1024) / 1024;

        return $min < $size;
    }

    public function validateMimes($file, $mimes) {}

    public function validateMimeTypes($file, ...$mimeTypes)
    {
        $mime = $file->getClientMimeType();

        return in_array($mime, $mimeTypes);
    }

    protected function getValidator($rule)
    {
        $validatorMethod = "validate" . ucfirst($rule);

        $reflectClass = new ReflectionClass(static::class);
        $methods = $reflectClass->getMethods(ReflectionMethod::IS_PUBLIC);

        foreach ($methods as $method) {
            if (strcasecmp($method->name, $validatorMethod) === 0) {
                // Trovato il nome del metodo corretto
                return $method->name;
            }
        }

        throw new Exception("Metodo $validatorMethod non trovato");
    }
}
