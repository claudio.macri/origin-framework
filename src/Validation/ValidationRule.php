<?php 

namespace Origin\Validation;

interface ValidationRule
{
    /**
     * Run the validation rule.
     *
     * @param string $attribute  // Il nome del campo che viene validato
     * @param mixed $value      // Il valore del campo da validare
     */
    public function validate(string $attribute, mixed $value): bool;
}