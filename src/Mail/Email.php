<?php 

namespace Origin\Mail;

use Origin\Configuration;
use Origin\Logger\Log;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

class Email
{
    protected $mail;
    public function __construct()
    {
        $mail = new PHPMailer(true);

        // Server settings
        $mail->IsSMTP();
        $mail->Host = Configuration::get("mail_host");
        $mail->SMTPAuth = true;
        $mail->Username = Configuration::get("mail_username");
        $mail->Password = Configuration::get("mail_password");
        $mail->SMTPSecure = PHPMailer::ENCRYPTION_SMTPS;
        $mail->Port = 465;

        $this->mail = $mail;
    }

    public static function send($from, $to, $subject, $message, $bcc = false, $confirm = false, $attachments = [])
    {
        Log::trace();

        try {
            $mail = (new static)->mail;

            // Recipients
            $mail->setFrom($from['email'], $from['name']);

            if ($to) {
                if (!is_array($to)) {
                    $to = array($to);
                }

                foreach ($to as $email) {
                    $mail->AddAddress($email);
                }
            }

            if ($bcc) {
                if (!is_array($bcc)) {
                    $bcc = array($bcc);
                }
                foreach ($bcc as $email) {
                    $mail->addBCC($email);
                }
            }

            if ($confirm) {
                $mail->ConfirmReadingTo = $from['email'];
            }

            if (array_key_exists('replyto', $from)) {
                $replyto = $from['replyto'];
                $mail->addReplyTo($replyto['email'], $replyto['name']);
            }

            // Attachments
            foreach ($attachments as $attach) {
                $isString = $attach['isString'];
                $filename = $attach['filename'];
                $output = $attach['output'] ?? null;
                if ($isString) {
                    $stringAttachment = $attach['string_attachment'];
                    $mail->addStringAttachment($stringAttachment, $filename);
                } else {
                    $mail->AddAttachment($filename, $output ?? $filename);
                }
            }

            // Content
            $mail->IsHTML(true);
            $mail->CharSet = PHPMailer::CHARSET_UTF8;
            $mail->Subject = $subject;
            $mail->Body = $message;

            $mail->Send();
        } catch (\Exception $e) {
            Log::error("Mailer Error: " . $e->getMessage());
            return false;
        }

        return true;
    }
}