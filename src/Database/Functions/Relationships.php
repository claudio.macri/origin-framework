<?php

namespace Origin\Database\Functions;

use Origin\Database\Relations\Relation;
use ReflectionClass;

trait Relationships
{
    protected $relations = [];
    protected $touches = [];

    protected function hasOne($related, $foreignKey, $localKey = null)
    {
        $localKey ??= $this->getPrimaryKey();
        return Relation::hasOne($this, $related, $foreignKey, $localKey);
    }

    protected function hasMany($related, $foreignKey, $localKey = null)
    {
        $localKey ??= $this->getPrimaryKey();
        return Relation::hasMany($this, $related, $foreignKey, $localKey);
    }

    protected function belongsTo($parent, $foreignKey, $ownerKey = null)
    {
        $ownerKey ??= (new $parent)->getPrimaryKey();
        return Relation::belongsTo($this, $parent, $foreignKey, $ownerKey);
    }

    protected function belongsToMany($parent, $pivotTable, $foreignPivotKey, $relatedPivotKey, $parentKey = null, $relatedKey = null, $extraPivotAttributes = null)
    {
        $parentKey ??= (new $parent)->getPrimaryKey();
        $relatedKey ??= $this->getPrimaryKey();
        $extraPivotAttributes ??= [];
        return Relation::belongsToMany($this, $parent, $pivotTable, $foreignPivotKey, $relatedPivotKey, $parentKey, $relatedKey, $extraPivotAttributes);
    }

    protected function getRelationFunctions()
    {
        $classReflection = new ReflectionClass(static::class);
        $relations = [];

        foreach ($classReflection->getMethods() as $method) {
            // Verifica se il metodo ha un tipo di ritorno
            if ($method->hasReturnType()) {
                // Ottieni il nome della classe del tipo di ritorno
                $returnType = $method->getReturnType();
                $returnClassName = $returnType->getName();

                // Verifica se il tipo di ritorno è uguale al nome della classe target
                if (is_subclass_of($returnClassName, Relation::class)) {
                    $relations[] = $method->getName();
                }
            }
        }

        return $relations;
    }

    public function getRelations()
    {
        return $this->relations;
    }

    public function getTouches()
    {
        return $this->touches;
    }

    public function setRelation($relationName, $relation)
    {
        $this->relations[$relationName] = $relation;
    }

    public function setTouche($relation)
    {
        $this->touches[] = $relation;
    }
}
