<?php

namespace Origin\Database\Functions;

trait SoftDeletes
{
    protected $softDelete = true;
    protected $onDeleteCascade = false;

    public function forceDelete()
    {
        $dao = $this->newModelDAO();
        return $dao->delete($this);
    }

    public function trashed()
    {
        $dao = $this->newModelDAO();
        if ($this->softDelete) {
            return $dao->trashed();
        }

        return false;
    }

    public function hasSoftDelete()
    {
        return $this->softDelete;
    }
}
