<?php

namespace Origin\Database\Functions;

use Exception;

trait Attributes
{
    protected $attributes = [];
    protected $fields = [];

    public function fill($attributes)
    {
        foreach ($attributes as $key => $value) {
            $this->setAttribute($key, $value);
        }
    }

    public function toArray()
    {
        return $this->attributes;
    }

    public function toJSON()
    {
        return json_encode($this->attributes);
    }

    public function getFields()
    {
        return $this->fields;
    }

    public function getAttributes()
    {
        return $this->attributes;
    }

    public function getAttribute($key)
    {
        if (in_array($key, $this->fields) || $key == $this->primaryKey) {
            return $this->attributes[$key];
        }

        if (in_array($key, $this->relations)) {
            $relation = $this->$key();

            return $relation->getResults();
        }

        throw new Exception("$key not found", 1);
    }

    public function setAttribute($key, $value)
    {
        if (in_array($key, $this->fields) || $key == $this->primaryKey) {
            $this->attributes[$key] = $value;
        } else {
            // throw new Exception("$key not found", 1);
        }
    }
}
