<?php

namespace Origin\Database\Relations;

use Origin\Database\DAO;

class HasOne extends Relation
{
    protected $relation =  "hasOne";
    protected $foreignKey;
    protected $localKey;

    public function getResults()
    {
        $related = new $this->related;
        $relTable = $related->getTable();
        $foreignKey = $relTable . "." . $this->foreignKey;

        $parent = $this->model;
        $parTable = $parent->getTable();
        $localKey = $parTable . "." . $this->localKey;
        $primaryKey = $parTable . "." . $parent->getPrimaryKey();

        $sql = "SELECT $relTable.* FROM $parTable JOIN $relTable ON ($localKey=$foreignKey) WHERE $primaryKey=:id";
        $params = [
            "id" => $parent->id()
        ];

        if ($related->hasSoftDelete()) {
            $deleted_at = $relTable . "." . $related->deleted_at();
            $sql .= " AND $deleted_at IS NULL";
        }

        return DAO::query($sql)->setBindings($params)->first("model", $this->related);
    }

    public function save($relatedModel)
    {
        $id = $this->model->id();
        $relatedModel->setTouche([
            "relation" => "belongsTo",
            "foreignKey" => $this->foreignKey,
            "key" => $id
        ]);
        $relatedModel->save();
    }

    static function newInstance($model, $related, $foreignKey, $localKey)
    {
        $relation = new static;
        $relation->model = $model;
        $relation->parent = get_class($model);
        $relation->related = $related;
        $relation->foreignKey = $foreignKey;
        $relation->localKey = $localKey;

        return $relation;
    }

    function getForeignKey()
    {
        return $this->foreignKey;
    }

    function getLocalKey()
    {
        return $this->localKey;
    }
}
