<?php

namespace Origin\Database\Relations;

use Origin\Database\DAO;

class BelongsToMany extends Relation
{
    protected $relation =  "belongsToMany";
    protected $pivotTable;
    protected $foreignPivotKey; // la chiave esterna associato al parent nella tabella pivot
    protected $relatedPivotKey; // la chiave esterna associato al related nella tabella pivot
    protected $parentKey = "id";
    protected $relatedKey = "id";
    protected $extraPivotAttributes = [];


    public function getResults()
    {
        $table = $this->pivotTable;
        $foreignPivotKey = $table . "." . $this->foreignPivotKey;
        $relatedPivotKey = $table . "." . $this->relatedPivotKey;

        $related = $this->model;
        $relTable = $related->getTable();
        $relatedKey = "r." . $this->relatedKey;
        $primaryKey = "r." . $related->getPrimaryKey();

        $parent = new $this->parent;
        $parTable = $parent->getTable();
        $parentKey = "p." . $this->parentKey;

        $sql = "SELECT p.* FROM $table JOIN $relTable r ON ($relatedPivotKey=$relatedKey) JOIN $parTable p ON ($foreignPivotKey=$parentKey) WHERE $primaryKey=:id";
        $params = [
            "id" => $related->id()
        ];

        if ($parent->hasSoftDelete()) {
            $deleted_at = "p." . $parent->deleted_at();
            $sql .= " AND $deleted_at IS NULL";
        }

        return DAO::query($sql)->setBindings($params)->get("model", $this->parent);
    }

    public function withExtraAttributes(...$attributes)
    {
        $table = $this->pivotTable;
        $foreignPivotKey = $table . "." . $this->foreignPivotKey;
        $relatedPivotKey = $table . "." . $this->relatedPivotKey;

        $related = $this->model;
        $relTable = $related->getTable();
        $relatedKey = "r." . $this->relatedKey;
        $primaryKey = "r." . $related->getPrimaryKey();

        $parent = new $this->parent;
        $parTable = $parent->getTable();
        $parentKey = "p." . $this->parentKey;

        // Aggiungi il prefisso a ogni elemento dell'array
        $extraAttributes = array_map(function ($attr) use ($table) {
            return "$table.$attr";
        }, $attributes);

        // SELECT
        $select = "p.*," . implode(",", $extraAttributes);

        $sql = "SELECT $select FROM $table JOIN $relTable r ON ($relatedPivotKey=$relatedKey) JOIN $parTable p ON ($foreignPivotKey=$parentKey) WHERE $primaryKey=:id";
        $params = [
            "id" => $related->id()
        ];

        if ($parent->hasSoftDelete()) {
            $deleted_at = "p." . $parent->deleted_at();
            $sql .= " AND $deleted_at IS NULL";
        }

        return DAO::query($sql)->setBindings($params)->get();
    }

    public function attach(...$args)
    {
        $id = $args[0];
        $extraAttributes = [];

        if (count($args) === 2 && is_array($args[1])) {
            $extraAttributes = $args[1];
        } elseif (count($args) === 1) {
            if (is_array($id)) {
                $result = false;
                foreach ($id as $key => $value) {
                    if (is_array($value)) {
                        $result = $this->attach($key, $value);
                    } else {
                        $result = $this->attach($value);
                    }

                    if (!$result) {
                        break;
                    }
                }
                return $result;
            }
        }

        $attributes = [
            $this->relatedPivotKey => $this->model->{$this->relatedKey},
            $this->foreignPivotKey => $id,
            ...$extraAttributes
        ];

        $columns = implode(', ', array_keys($attributes));
        $placeholders = ':' . implode(', :', array_keys($attributes));

        $sql = "INSERT INTO {$this->pivotTable} ($columns) VALUES ($placeholders)";

        return DAO::query($sql)->setBindings($attributes)->execute();
    }

    public function detach(...$ids)
    {
        $pivotTable = $this->pivotTable;
        $foreignPivotKey = $this->foreignPivotKey;
        $relatedPivotKey = $this->relatedPivotKey;

        $model = $this->model;

        $result = false;
        if (!empty($ids)) {
            foreach ($ids as $id) {

                $sql = "DELETE FROM {$pivotTable} WHERE {$foreignPivotKey}=:{$foreignPivotKey} AND {$relatedPivotKey}=:{$relatedPivotKey}";

                $params = [
                    $relatedPivotKey => $model->{$this->relatedKey},
                    $foreignPivotKey => $id
                ];

                $result = DAO::query($sql)->setBindings($params)->execute();

                if (!$result) {
                    break;
                }
            }
        } else {
            $sql = "DELETE FROM {$pivotTable} WHERE {$relatedPivotKey}=:{$relatedPivotKey}";

            $params = [
                $relatedPivotKey => $model->{$this->relatedKey}
            ];

            $result = DAO::query($sql)->setBindings($params)->execute();
        }


        return $result;
    }

    static function newInstance($model, $parent, $pivotTable, $foreignPivotKey, $relatedPivotKey, $parentKey, $relatedKey, $extraPivotAttributes)
    {
        $relation = new static;
        $relation->model = $model;
        $relation->parent = $parent;
        $relation->related = get_class($model);
        $relation->pivotTable = $pivotTable;
        $relation->foreignPivotKey = $foreignPivotKey;
        $relation->relatedPivotKey = $relatedPivotKey;
        $relation->parentKey = $parentKey;
        $relation->relatedKey = $relatedKey;
        $relation->extraPivotAttributes = $extraPivotAttributes;

        return $relation;
    }

    function getPivotTable()
    {
        return $this->pivotTable;
    }

    function getForeignPivotKey()
    {
        return $this->foreignPivotKey;
    }

    function getRelatedPivotKey()
    {
        return $this->relatedPivotKey;
    }

    function getParentKey()
    {
        return $this->parentKey;
    }

    function getRelatedKey()
    {
        return $this->relatedKey;
    }

    function getExtraPivotAttributes()
    {
        return $this->extraPivotAttributes;
    }
}
