<?php

namespace Origin\Database\Relations;

use Origin\Database\DAO;

class BelongsTo extends Relation
{
    protected $relation =  "belongsTo";
    protected $foreignKey;
    protected $ownerKey;

    public function getResults()
    {
        $related = $this->model;
        $relTable = $related->getTable();
        $foreignKey = "r." . $this->foreignKey;
        $primaryKey = "r." . $related->getPrimaryKey();

        $parent = new $this->parent;
        $parTable = $parent->getTable();
        $ownerKey = "p." . $this->ownerKey;

        $sql = "SELECT p.* FROM $relTable r JOIN $parTable p ON ($foreignKey=$ownerKey) WHERE $primaryKey=:id";
        $params = [
            "id" => $related->id()
        ];

        return DAO::query($sql)->setBindings($params)->first("model", $this->parent);
    }

    public function associate($related)
    {
        $this->model->setTouche([
            "relation" => $this->relation,
            "foreignKey" => $this->foreignKey,
            "key" => $related->id()
        ]);
    }

    public function dissociate()
    {
        $this->model->setTouche([
            "relation" => $this->relation,
            "foreignKey" => $this->foreignKey,
            "key" => null
        ]);

        return $this->model->save();
    }

    function getForeignKey()
    {
        return $this->foreignKey;
    }

    function getOwnerKey()
    {
        return $this->ownerKey;
    }

    static function newInstance($model, $parent, $foreignKey, $ownerKey)
    {
        $relation = new static;
        $relation->model = $model;
        $relation->parent = $parent;
        $relation->related = get_class($model);
        $relation->foreignKey = $foreignKey;
        $relation->ownerKey = $ownerKey;

        return $relation;
    }
}
