<?php

namespace Origin\Database\Relations;

use Origin\Logger;

abstract class Relation
{
    protected $relation;
    protected $model;
    protected $parent;
    protected $related;

    abstract public function getResults();

    public static function hasOne($model, $related, $foreignKey, $localKey)
    {
        return HasOne::newInstance($model, $related, $foreignKey, $localKey);
    }

    public static function belongsTo($model, $parent, $foreignKey, $ownerKey)
    {
        return BelongsTo::newInstance($model, $parent, $foreignKey, $ownerKey);
    }

    public static function hasMany($model, $related, $foreignKey, $localKey)
    {
        return HasMany::newInstance($model, $related, $foreignKey, $localKey);
    }

    public static function belongsToMany($model, $parent, $pivotTable, $foreignPivotKey, $relatedPivotKey, $parentKey, $relatedKey, $pivotExtraAttributes = [])
    {
        return BelongsToMany::newInstance($model, $parent, $pivotTable, $foreignPivotKey, $relatedPivotKey, $parentKey, $relatedKey, $pivotExtraAttributes);
    }
}
