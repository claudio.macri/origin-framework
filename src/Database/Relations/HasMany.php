<?php

namespace Origin\Database\Relations;

use Origin\Database\DAO;

class HasMany extends Relation
{
    protected $relation =  "hasMany";
    protected $foreignKey;
    protected $localKey;

    public function getResults()
    {
        $related = new $this->related;
        $relTable = $related->getTable();
        $foreignKey = "r." . $this->foreignKey;

        $parent = $this->model;
        $parTable = $parent->getTable();
        $localKey = "p." . $this->localKey;
        $primaryKey = "p." . $parent->getPrimaryKey();

        $sql = "SELECT r.* FROM $parTable p JOIN $relTable r ON ($localKey=$foreignKey) WHERE $primaryKey=:id";
        $params = [
            "id" => $parent->id()
        ];

        if ($related->hasSoftDelete()) {
            $deleted_at = "r." . $related->deleted_at();
            $sql .= " AND $deleted_at IS NULL";
        }

        return DAO::query($sql)->setBindings($params)->get("model", $this->related);
    }

    public function save(...$relatedModels)
    {
        foreach ($relatedModels as $rel) {
            $id = $this->model->id();
            $rel->setTouche([
                "relation" => "belongsTo",
                "foreignKey" => $this->foreignKey,
                "key" => $id
            ]);
            $rel->save();
        }
    }

    function getForeignKey()
    {
        return $this->foreignKey;
    }

    function getLocalKey()
    {
        return $this->localKey;
    }

    static function newInstance($model, $related, $foreignKey, $localKey)
    {
        $relation = new static;
        $relation->model = $model;
        $relation->parent = get_class($model);
        $relation->related = $related;
        $relation->foreignKey = $foreignKey;
        $relation->localKey = $localKey;

        return $relation;
    }
}
