<?php

namespace Origin\Database;

use Exception;
use Origin\Database\Contracts\Repository as RepositoryContract;

class Repository implements RepositoryContract
{
    protected $model;
    private $builder;

    public function findById(int $id)
    {
        return $this->model->find($id);
    }

    public function getAll()
    {
        return $this->model->all();
    }

    public function getByIds(array $ids): array
    {
        if (empty($ids)) {
            return [];
        }

        $in = implode(',', $ids);
        $sql = "SELECT * FROM {$this->model->getTable()} WHERE id IN ($in)";

        return $this->query($sql)->get('model');
    }

    public function create(array $data)
    {
        try {
            $this->model->fill($data);

            if (!$this->model->save()) {
                throw new Exception('Creazione fallita!');
            }

            return $this->model;
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function update(int $id, array $data)
    {
        try {
            $this->model = $this->findById($id);
            $this->model->fill($data);

            if (!$this->model->save()) {
                throw new Exception("Aggiornamento fallito");
            }

            return $this->model;
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function delete(int $id)
    {
        Connection::beginTransaction();

        try {
            $this->model = $this->findById($id);

            if (!$this->model->delete()) {
                throw new Exception("Eliminazione fallita");
            }

            Connection::commit();

            return true;
        } catch (Exception $e) {
            Connection::rollBack();
            throw $e;
        }
    }

    public function setModel($model)
    {
        $this->model = $model;
        $this->builder = $model->newBuilder();
    }

    public function query($sql, array $params = [])
    {
        return $this->builder->query($sql, $params);
    }
}
