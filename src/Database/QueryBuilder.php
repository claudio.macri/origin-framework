<?php

namespace Origin\Database;

class QueryBuilder
{
    protected $connection;
    protected $query;
    protected $bindings = [];
    protected $table;
    protected $model;
    protected $resultType = "array";
    protected $skip = false;

    function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    function first($resultType = null, $param = null)
    {
        $this->query .= " LIMIT 1";
        $results = $this->get($resultType, $param);
        return !empty($results) ? $results[0] : false;
    }

    function take($value, $resultType = null, $param = null)
    {
        if ($value) {
            $this->query .= " LIMIT ";

            if ($this->skip !== false) {
                $this->query .= $this->skip . ", ";
            }

            $this->query .= $value;
        }

        return $this->get($resultType, $param);
    }

    function get($resultType = null, $param = null): ?array
    {
        $resultType = $resultType ?? $this->resultType;
        $results = $this->getConnection()->performSelect($this->query, $this->bindings);

        switch ($resultType) {
            case "array":
                return $results;
            case "model":
                $classModel = $param ?? $this->model;
                if (class_exists($classModel)) {
                    // Se la classe esiste, crea un'istanza della classe e restituiscila
                    $models = [];
                    foreach ($results as $result) {
                        $models[] = new $classModel($result);
                    }
                    return $models;
                } else {
                    // Se la classe non esiste, restituisci null
                    return null;
                }
            case "column":
                $columnIndex = $param ?? 0;
                $values = [];
                foreach ($results as $result) {

                    if (is_string($columnIndex)) {
                        // Se l'indice è una stringa, restituisci il valore associato a quella chiave
                        $value = isset($result[$columnIndex]) ? $result[$columnIndex] : null;
                    } elseif (is_numeric($columnIndex)) {
                        // Se l'indice è numerico, cerca l'indice in base alla posizione numerica
                        $keys = array_keys($result);
                        $value = isset($keys[$columnIndex]) ? $result[$keys[$columnIndex]] : null;
                    } else {
                        // Se l'indice non è né una stringa né un numero, restituisci null
                        $value = null;
                    }
                    if (isset($value)) {
                        $values[] = $value;
                    }
                }
                return $values;
            default:
                return null;
        }
    }

    function execute()
    {
        return $this->getConnection()->execute($this->query, $this->bindings);
    }

    function getById($key, $deleted = null)
    {
        $primaryKey = array_key_first($key);
        $sql = "SELECT * FROM {$this->table} WHERE $primaryKey=:$primaryKey";

        if (isset($deleted)) {
            $sql .= " AND $deleted IS NULL";
        }
        $results = $this->getConnection()->performSelect($sql, $key);

        return !empty($results) ? $results[0] : false;
    }

    function getAll($deleted = null)
    {
        $sql = "SELECT * FROM {$this->table}";

        if (isset($deleted)) {
            $sql .= " WHERE $deleted IS NULL";
        }

        return $this->getConnection()->performSelect($sql);
    }

    function getWhere(array $wheres = [], $resultType = null, $param = null)
    {
        $this->query = "SELECT * FROM {$this->table}";
        foreach ($wheres as $key => $value) {
            $this->where($key, $value);
        }
        return $this->get($resultType, $param);
    }

    function insert($values)
    {
        $columns = implode(', ', array_keys($values));
        $placeholders = ':' . implode(', :', array_keys($values));

        $sql = "INSERT INTO {$this->table} ($columns) VALUES ($placeholders)";

        return $this->getConnection()->performInsert($sql, $values);
    }

    function update($key, $values)
    {
        $updateValues = [];
        foreach ($values as $column => $value) {
            $updateValues[] = "$column = :$column";
        }
        $updateValues = implode(', ', $updateValues);

        $values += $key;

        $primaryKey = array_key_first($key);

        $sql = "UPDATE {$this->table} SET $updateValues WHERE $primaryKey = :$primaryKey";
        return $this->getConnection()->performUpdate($sql, $values);
    }

    function delete($key)
    {
        $primaryKey = array_key_first($key);
        $sql = "DELETE FROM {$this->table} WHERE $primaryKey=:$primaryKey";
        return $this->getConnection()->performDelete($sql, $key);
    }

    function query($sql, array $bindings = [])
    {
        $this->query = $sql;
        $this->bindings = $bindings;

        return $this;
    }

    function where($key, $value, $operator = "=")
    {
        if (stripos($this->query, "WHERE") !== false) {
            $this->query .= " AND ";
        } else {
            $this->query .= " WHERE ";
        }
        $this->query .= "$key=:$key";

        $this->addBinding($key, $value);
    }

    function skip($value)
    {
        $this->skip = $value;
        return $this;
    }

    public function setBindings(array $bindings)
    {
        $this->bindings = $bindings;

        return $this;
    }

    public function addBinding($name, $value)
    {
        $this->bindings[$name] = $value;

        return $this;
    }

    public function getBindings()
    {
        return $this->bindings;
    }

    public function setTable($table)
    {
        $this->table = $table;

        return $this;
    }

    public function setModel($model)
    {
        $this->model = $model;

        return $this;
    }

    public function getConnection()
    {
        return $this->connection;
    }

    public function debug()
    {
        echo 'Model: ' . $this->model . "\n";
        echo 'Query: ' . $this->query . "\n";
        echo 'Params: ' . json_encode($this->bindings, JSON_PRETTY_PRINT);
    }
}
