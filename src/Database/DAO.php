<?php

namespace Origin\Database;

use Origin\Logger\Log;

class DAO
{
    protected $query; // query builder per accedere al database   
    protected $model;

    public static function factory($model)
    {
        $query = $model->getConnection()->query();
        $classModelDAO = get_class($model) . "DAO";

        $dao = new $classModelDAO($query);
        $dao->setModel($model);

        return $dao;
    }

    public function __construct($query)
    {
        $this->query = $query; 
    }

    public function getById($id)
    {
        $table = $this->model->getTable();

        $key = [
            $this->model->getPrimaryKey() => $id
        ];

        $this->query->setTable($table);

        if ($this->model->hasSoftDelete()) {
            $deleted = $this->model->deleted_at();
        }

        $result = $this->query->getById($key, $deleted ?? null);

        if ($result) {
            $this->model->fill($result);

            return $this->model;
        }

        return false;
    }

    public function getAll()
    {        
        $classModel = get_class($this->model);
        $table = $this->model->getTable();        

        $this->query->setTable($table);

        if ($this->model->hasSoftDelete()) {
            $deleted = $this->model->deleted_at();
        }

        $results = $this->query->getAll($deleted ?? null);

        $models = [];
        foreach ($results as $attributes) {
            $models[] = new $classModel($attributes);
        }

        return $models;
    }

    public function insertOrUpdate(&$model)
    {
        $id = $model->id();
        if (!isset($id)) {
            return $this->insert($model);
        } else {
            return $this->update($model);
        }
    }

    public function insert(&$model)
    {
        Log::trace();

        $table = $model->getTable();
        $attributes = array_intersect_key($model->getAttributes(), array_flip($model->getFields()));
       
        if (!empty($model->getTouches())) {
            foreach ($model->getTouches() as $r => $relation) {
                if ($relation["relation"] == "belongsTo") {
                    $attributes[$relation["foreignKey"]] = $relation["key"];
                }
            }
        }

        $this->query->setTable($table);

        if (!($id = $this->query->insert($attributes))) {
            return false;
        }

        $primaryKey = $model->getPrimaryKey();
        $model->$primaryKey = $id;

        return true;
    }

    public function update($model)
    {
        Log::trace();

        $table = $model->getTable();
        $attributes = array_intersect_key($model->getAttributes(), array_flip($model->getFields()));        
        
        if (!empty($model->getTouches())) {
            foreach ($model->getTouches() as $r => $relation) {
                if ($relation["relation"] == "belongsTo") {
                    $attributes[$relation["foreignKey"]] = $relation["key"];
                }
            }
        }

        $key = [
            $model->getPrimaryKey() => $model->id()
        ];

        $this->query->setTable($table);

        return $this->query->update($key, $attributes);
    }   

    public function delete($model)
    {
        Log::trace();

        $table = $model->getTable();
        $primaryKey = $model->getPrimaryKey();       

        $key = [
            $primaryKey => $model->id()
        ];

        $this->query->setTable($table);

        return $this->query->delete($key);
    }

    public function softDelete($model)
    {
        Log::trace();

        $table = $model->getTable();

        $deleted = [
            $model->deleted_at() => date("Y-m-d H:i:s")
        ];

        $key = [
            $model->getPrimaryKey() => $model->id()
        ];

        $this->query->setTable($table);

        return $this->query->update($key, $deleted);
    }

    public function trashed()
    {                
        $table = $this->model->getTable();

        $deleted = $this->model->deleted_at();
        
        $key = [
            $this->model->getPrimaryKey() => $this->model->id()
        ];

        $this->query->setTable($table);
        $result = $this->query->getById($key);

        if ($result) {
            return array_key_exists($deleted, $result) && isset($result[$deleted]);
        }

        return true;
    }

    public function getModel()
    {
        return $this->model;
    }

    public function setModel($model)
    {
        $this->model = $model;

        if ($this->query) {
            $this->query->setTable($model->getTable());
        }

        return $this;
    }

    public static function startTransaction()
    {
        Connection::beginTransaction();
    }

    public static function commitTransaction()
    {
        Connection::commit();
    }

    public static function rollbackTransaction()
    {
        Connection::rollBack();
    }

    public static function query($sql)
    {        
        $model = new Model;
        $query = $model->newBuilder();
        return $query->query($sql);
    }
}
