<?php

namespace Origin\Database\Contracts;

interface Repository
{
    public function findById(int $id);
    public function getAll();
    public function getByIds(array $ids);
    public function create(array $data);
    public function update(int $id, array $data);
    public function delete(int $id);
}
