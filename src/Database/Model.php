<?php

namespace Origin\Database;

use Exception;
use Origin\Database\Functions\Attributes;
use Origin\Database\Functions\Relationships;
use Origin\Database\Functions\SoftDeletes;

class Model
{
    use Attributes;
    use SoftDeletes;
    use Relationships;

    protected $table;
    protected $primaryKey = "id";
    protected $timestamp = true;
    const CREATED_AT = "created_at";
    const UPDATED_AT = "updated_at";
    const DELETED_AT = "deleted_at";

    public function __construct(array $attributi = [])
    {
        $this->fill($attributi);

        $this->relations = $this->getRelationFunctions();
    }

    public static function find($id)
    {
        $model = new static;
        $dao = $model->newModelDAO();
        return $dao->getById($id);
    }

    public static function all()
    {
        $model = new static;
        $dao = $model->newModelDAO();
        return $dao->getAll();
    }

    public function save()
    {
        $dao = $this->newModelDAO();
        return $dao->insertOrUpdate($this);
    }

    public static function create($attributes)
    {
        $model = new static($attributes);
        $model->save();
        return $model;
    }

    public function delete()
    {
        $dao = $this->newModelDAO();
        return $this->softDelete ? $dao->softDelete($this) : $dao->delete($this);
    }

    public static function destroy(...$ids)
    {
        foreach ($ids as $id) {
            $model = self::find($id);
            if ($model) {
                $model->forceDelete();
            }
        }
    }

    public static function query($sql)
    {
        return (new static)->newBuilder()->query($sql);
    }

    public function id()
    {
        return $this->attributes[$this->primaryKey] ?? null;
    }

    public function getTable()
    {
        return $this->table;
    }

    public function getPrimaryKey()
    {
        return $this->primaryKey;
    }

    public function created_at()
    {
        return static::CREATED_AT;
    }

    public function updated_at()
    {
        return static::UPDATED_AT;
    }

    public function deleted_at()
    {
        return static::DELETED_AT;
    }

    public function newModelDAO()
    {
        return $this->newDAO($this->newBuilder())->setModel($this);
    }

    public function newDAO($query)
    {
        return new DAO($query);
    }

    public function newBuilder()
    {
        return $this->getConnection()->query()->setTable($this->getTable())->setModel(get_class($this));
    }

    public function getConnection()
    {
        return Connection::getConnection();
    }

    public function __toString()
    {
        return $this->toJSON();
    }

    public function __get($key)
    {
        return $this->getAttribute($key);
    }

    public function __set($key, $value)
    {
        $this->setAttribute($key, $value);
    }
}
