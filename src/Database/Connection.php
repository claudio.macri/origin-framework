<?php

namespace Origin\Database;

use Origin\Config;
use Origin\Configuration;
use Origin\Logger\Log;
use PDO;
use PDOException;

class Connection
{
    protected static $conn = null;
    protected $pdo;

    protected function __construct()
    {
        $db_config = Config::get("database.connections")[Config::get("database.default")];

        $this->connect($db_config['host'], $db_config['port'], $db_config['database'], $db_config['username'], $db_config['password']);
    }

    public function getPDO(): PDO
    {
        return $this->pdo;
    }

    public function connect($host, $port, $name, $user, $pass)
    {
        if (self::$conn) {
            $this->pdo = self::$conn->getPDO();

            return true;
        }

        try {
            $this->pdo = new PDO("mysql:host=" . $host . ";port=" . $port . ";dbname=" . $name . ";charset=utf8mb4", $user, $pass);
            $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            Log::info('connessione col database ' . $name . ' stabilito ');
            return true;
        } catch (PDOException $e) {
            echo $e->getMessage();
            Log::error('errore connessione col database ' . $name);
            Log::error($e->getMessage());
            return false;
        }
    }

    public function performSelect($query, $bindings = null)
    {
        try {
            $stmt = $this->pdo->prepare($query);
            $stmt->execute($bindings);

            return $stmt->fetchAll(PDO::FETCH_ASSOC);
        } catch (PDOException $e) {
            echo $e->getMessage();
            Log::error($e->getMessage());
            return false;
        }
    }

    public function performInsert($query, $values)
    {
        try {
            $stmt = $this->pdo->prepare($query);
            $stmt->execute($values);

            return $stmt->rowCount() > 0 ? $this->pdo->lastInsertId() : false;
        } catch (PDOException $e) {
            echo $e->getMessage();
            Log::error($e->getMessage());
            return false;
        }
    }

    public function performUpdate($query, $values)
    {
        try {
            $stmt = $this->pdo->prepare($query);
            $stmt->execute($values);

            return /* $stmt->rowCount() > 0 */true;
        } catch (PDOException $e) {
            echo $e->getMessage();
            Log::error($e->getMessage());
            return false;
        }
    }

    public function performDelete($query, $key)
    {
        try {
            $stmt = $this->pdo->prepare($query);
            $stmt->execute($key);

            return $stmt->rowCount() > 0;
        } catch (PDOException $e) {
            echo $e->getMessage();
            Log::error($e->getMessage());
            return false;
        }
    }

    public function execute($query, $bindings = null) {
        try {
            $stmt = $this->pdo->prepare($query);
            $stmt->execute($bindings);

            return $stmt->rowCount() > 0;
        } catch (PDOException $e) {
            echo $e->getMessage();
            Log::error($e->getMessage());
            return false;
        }
    }

    public static function beginTransaction()
    {
        Log::info("Aperta transazione con il database");

        if (self::$conn) {
            self::$conn->getPDO()->beginTransaction();
        }
    }

    public static function commit()
    {
        Log::info("Chiusa transazione con il database");

        if (self::$conn) {
            self::$conn->getPDO()->commit();
        }
    }

    public static function rollBack()
    {
        Log::error("Transazione con il database fallita");

        if (self::$conn) {
            self::$conn->getPDO()->rollBack();
        }
    }

    public function query()
    {
        return new QueryBuilder($this);
    }

    public function table($table)
    {
        $query = $this->query();
        $query->setTable($table);
        return $query;
    }

    public static function getConnection()
    {
        if (!self::$conn) {
            self::$conn = new static;
        }

        return self::$conn;
    }
}
