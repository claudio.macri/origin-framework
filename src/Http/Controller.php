<?php

namespace Origin\Http;

use Exception;
use Origin\Logger;
use Origin\Routing\Router;

class Controller
{
    protected $middleware = [];

    protected function middleware($middleware, ...$args)
    {
        $loadedMiddleware = Router::getInstance()->getMiddleware();

        list($middleware, $argument) = strpos($middleware, ":") !== false ? explode(":", $middleware) : [$middleware, null];

        if (class_exists($middleware)) {
            $middleware = [
                "middleware" => $middleware,
                "args" => null
            ];
        } else if (array_key_exists($middleware, $loadedMiddleware)) {
            $middleware = [
                "middleware" => $loadedMiddleware[$middleware],
                "args" => $argument
            ];
        } else {
            $middleware = null;
        }

        if (null !== $middleware) {
            $this->middleware[] = $middleware;
        }
    }

    public function getMiddleware()
    {
        return $this->middleware;
    }

    public function __set($name, $value)
    {
        if (property_exists($this, $name)) {
            $this->$name = $value;
        } else {
            throw new Exception("La proprietà $name non esiste.");
        }
    }
}
