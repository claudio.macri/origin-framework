<?php

namespace Origin\Http;

use Origin\Logger\Log;

class Session
{
    /**
     * Session data
     */
    protected $attributes = [];
    protected static $instance;

    public function __construct()
    {
        $this->loadVariables();
    }

    /**
     * Get singleton instance
     */
    public static function getInstance(): Session
    {
        if (is_null(static::$instance)) {
            static::$instance = new static();
        }

        return static::$instance;
    }

    /**
     * Controlla se la sessione è attiva
     */
    public function isStarted()
    {
        return \PHP_SESSION_ACTIVE === session_status();
    }

    /**
     * Check statico dello stato sessione
     */
    public static function checkStatus()
    {
        return static::getInstance()->isStarted();
    }

    /**
     * Avvia la sessione
     */
    public function start()
    {
        if ($this->isStarted()) {
            return true;
        }

        if (session_start()) {
            $this->loadVariables();
            return true;
        }

        return false;
    }

    /**
     * Check if key exists
     */
    public function has($name)
    {
        return array_key_exists($name, $this->attributes);
    }

    /**
     * Get value
     */
    public function get($name)
    {
        return $this->has($name) ? $this->attributes[$name] : null;
    }

    /**
     * Set value
     */
    public function set($name, $value)
    {
        $this->attributes[$name] = $value;
        return $this;
    }

    /**
     * Remove value
     */
    public function remove($name)
    {
        if ($this->has($name)) {
            unset($this->attributes[$name]);
        }
    }

    /**
     * Clear all data
     */
    public function clear()
    {
        $this->attributes = [];
    }

    /**
     * Invalidate session
     */
    public function invalidate()
    {
        $this->clear();
        $this->regenerate(true);
    }

    /**
     * Regenerate session id
     */
    public function regenerate($delete_old_session = false)
    {
        session_regenerate_id($delete_old_session);
    }

    /**
     * Get all session data
     */
    public function all()
    {
        return $this->attributes;
    }

    /**
     * Get session id
     */
    public function id()
    {
        return session_id();
    }

    /**
     * Get session name
     */
    public function name()
    {
        return session_name();
    }

    // Static helper methods
    public static function getVariable($name, $default = null)
    {
        $session = self::getInstance();
        return $session->has($name) ? $session->get($name) : $default;
    }

    public static function setVariable($name, $value)
    {
        $session = self::getInstance();
        $session->set($name, $value);
    }

    public static function removeVariable($name)
    {
        $session = self::getInstance();
        $session->remove($name);
    }

    public static function forget($names)
    {
        $session = self::getInstance();

        if (is_string($names)) {
            $names = func_get_args();
        }

        foreach ($names as $name) {
            $session->remove($name);
        }
    }

    public static function hasVariable($name)
    {
        $session = self::getInstance();
        return $session->has($name);
    }

    /**
     * Carica le variabili di sessione
     */
    private function loadVariables()
    {
        if (empty($this->attributes)) {
            $this->attributes = &$_SESSION ?? [];
        }
    }
}
