<?php

namespace Origin\Http\Contracts;

use Origin\Http\Request;

interface DataTable
{
    public function data(Request $request);
}