<?php

namespace Origin\Http\Contracts;

use Origin\Http\Request;

interface ResourceController
{
    public function index();
    public function create();
    public function store(Request $request);
    public function edit($id);
    public function update(Request $request, $id);
    public function delete($id);
}
