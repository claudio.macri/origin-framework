<?php 

namespace Origin\Http;

use Origin\Application;

class Kernel
{
    protected $app;
    protected $dispatcher;
    protected $middleware = [];
    protected $middlewareGroups = [];
    protected $middlewareAliases = [];

    public function __construct(Application $app)
    {
        $this->app = $app;
        $this->dispatcher = new Dispatcher($this->middleware, $this->middlewareGroups, $this->middlewareAliases);
    }

    public function __invoke($request)
    {
        return $this->dispatcher->dispatch($request);
    }

    public function terminate($request, $response)
    {
        $this->dispatcher->terminate($request, $response);
    }
}