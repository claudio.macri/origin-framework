<?php

namespace Origin\Http;

class Cookie
{
    public static function make($name, $value, $minutes = 60)
    {
        $time = time() + ((int) $minutes * 60);

        setcookie($name, $value, $time, "/");
    }

    public static function destroySessionCookie($session)
    {
        // Cancella il cookie della sessione
        if (ini_get("session.use_cookies")) {
            $params = session_get_cookie_params();
            setcookie(
                $session,
                '',
                time() - 42000,
                $params["path"],
                $params["domain"],
                $params["secure"],
                $params["httponly"]
            );
        }
    }
}
