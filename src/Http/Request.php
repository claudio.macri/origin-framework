<?php

namespace Origin\Http;

use Origin\Auth\Auth;
use Origin\Config;
use Origin\Configuration;

class Request
{
    protected $debug = false;

    protected $query;
    protected $params;
    protected $files;
    protected $server;
    protected $session;
    protected $cookies;
    protected $headers;

    protected $uri;
    protected $url;
    protected $method;
    protected $pathInfo;

    protected $user;
    protected $route;

    protected $inputs = [];

    public static function capture()
    {
        $server = $_SERVER;
        $cookies = $_COOKIE;
        $query = $_GET;
        $params = $_POST;
        $files = $_FILES;
        $request = new static($query, $params, $files, $server, $cookies);
        return $request;
    }

    public static function create($uri, $method, $query = [], $params = [], $files = [])
    {
        $request = new static($query, $params, $files);

        $request->setMethod($method);
        $request->setUri($uri);
        return $request;
    }

    public function __construct(array $query = [], array $params = [], array $files = [], array $server = [], array $cookies = [])
    {
        $this->query = new ParameterContainer($query);
        $this->params = new ParameterContainer($params);
        $this->files = new File($files);
        $this->server = new Server($server);
        $this->cookies = new ParameterContainer($cookies);
        $this->headers = new ParameterContainer($this->server->getHeaders());

        // METHOD
        $this->method = $this->getMethod();
    }

    public function user()
    {
        return Auth::user();
    }

    public function route()
    {
        return $this->route;
    }

    public function input($name)
    {
        if ($this->query->has($name)) {
            return $this->query->get($name);
        }
        if ($this->params->has($name)) {
            return $this->params->get($name);
        }

        return null;
    }

    public function file($name)
    {
        if ($this->files->has($name)) {
            return $this->files->get($name);
        }

        return null;
    }

    public function all()
    {
        return array_merge($this->getInputs(), $this->getFiles());
    }

    public function only(array $keys)
    {
        $data = [];
        foreach ($keys as $key) {
            if ($this->hasInput($key)) {
                $data[$key] = $this->input($key);
            }
        }

        return $data;
    }

    function ajax()
    {
        return $this->isXmlHttpRequest();
    }

    function csrfToken()
    {
        return $this->getCsrfToken();
    }

    /**
     * Get the bearer token from the request headers.
     *
     * @return string|null
     */
    public function bearerToken()
    {
        return $this->getBearerToken();
    }

    /**
     * Determine if current request URI matches pattern
     *
     * @param string|array $patterns
     * @return bool
     */
    public function is($patterns)
    {
        $path = $this->getPathInfo();

        foreach ((array) $patterns as $pattern) {
            if ($this->matchPattern($pattern, $path)) {
                return true;
            }
        }

        return false;
    }

    function hasInput($name)
    {
        return $this->query->has($name) || $this->params->has($name);
    }

    function hasFile($name)
    {
        return $this->files->has($name);
    }

    function pathInfo()
    {
        $url = $this->getUrl();

        $pathInfo = pathinfo($url);

        return array_filter(array_values($pathInfo));
    }

    function isPOST()
    {
        return $this->getMethod() == "POST";
    }

    function isGET()
    {
        return $this->getMethod() == "GET";
    }

    public function isXmlHttpRequest(): bool
    {
        $xRequestedWith = $this->headers->get('X-Requested-With') ?? $this->headers->get('X_REQUESTED_WITH');
        return 'XMLHttpRequest' == $xRequestedWith;
    }

    public function getCsrfToken()
    {
        $xCsrfToken = $this->headers->get('X-Csrf-Token') ?? $this->headers->get('X_CSRF_TOKEN');
        return $xCsrfToken;
    }

    public function getBearerToken()
    {
        $header = $this->headers->get('Authorization') ?? '';

        if (str_starts_with($header, 'Bearer ')) {
            return substr($header, 7);
        }

        return null;
    }

    function setRoute($route)
    {
        $this->route = $route;
    }

    function getMethod()
    {
        if (null !== $this->method) {
            return $this->method;
        }

        $this->method = strtoupper($this->server->get('REQUEST_METHOD') ?? 'GET');

        if ('POST' !== $this->method) {
            return $this->method;
        }

        $method = $this->params->get('_method') ?? $this->query->get('_method') ?? "POST";

        return $this->method = strtoupper($method);
    }

    function setMethod(string $method)
    {
        $this->method = strtoupper($method);
    }

    function getUri()
    {
        return $this->server->get("REQUEST_URI");
    }

    function setUri($uri)
    {
        $this->server->set("REQUEST_URI", $uri);
    }

    function getUrl()
    {
        if (null !== $this->url) {
            return $this->url;
        }

        $requestUri = $this->getUri();

        return $this->url = parse_url($requestUri, PHP_URL_PATH);
    }

    function getPathInfo()
    {
        if (null === ($requestUri = $this->getUrl())) {
            return '/';
        }

        if ('' !== $requestUri && '/' !== $requestUri[0]) {
            $requestUri = '/' . $requestUri;
        }

        if (null === ($baseUrl = Config::get('app.base_url'))) {
            return $requestUri;
        }

        $pathInfo = substr($requestUri, strlen($baseUrl));
        if (false === $pathInfo || '' === $pathInfo) {
            // If substr() returns false then PATH_INFO is set to an empty string
            return '/';
        }

        return $pathInfo;
    }

    /**
     * Match the pattern against the path
     *
     * @param string $pattern
     * @param string $path
     * @return bool
     */
    protected function matchPattern($pattern, $path)
    {
        // Se il pattern inizia con 'api/', controlla se il path inizia con 'api/'
        if (str_starts_with($pattern, 'api/')) {
            return str_starts_with($path, '/api/');
        }

        // Se il pattern termina con *, fa match con tutti i path che iniziano con il pattern
        if (str_ends_with($pattern, '*')) {
            $pattern = rtrim($pattern, '*');
            return str_starts_with($path, '/' . $pattern);
        }

        // Altrimenti confronta esattamente
        return $path === '/' . ltrim($pattern, '/');
    }

    function getInputs()
    {
        return array_merge($this->query->all(), $this->params->all());
    }

    function getFiles()
    {
        return $this->files->all();
    }

    public function getSession()
    {
        return $this->session;
    }

    public function setSession($session)
    {
        $this->session = $session;
    }

    function __get($key)
    {
        return $this->input($key) ?? $this->file($key);
    }

    function __set($key, $value)
    {
        $this->params->set($key, $value);
    }
}
