<?php

namespace Origin\Http;

use Exception;
use Origin\Application;
use Origin\Config;

class Response
{
    /**
     * HTTP status codes and their default messages
     */
    protected static $statusTexts = [
        400 => 'Bad Request',
        401 => 'Unauthorized',
        403 => 'Forbidden',
        404 => 'Not Found',
        405 => 'Method Not Allowed',
        419 => 'Page Expired',
        429 => 'Too Many Requests',
        500 => 'Internal Server Error',
        503 => 'Service Unavailable',
    ];

    /**
     * Throw an HttpException with the given data.
     *
     * @param  int  $code
     * @param  string|null  $message
     * @param  array  $headers
     * @return never
     *
     * @throws \Exception
     */
    public static function abort($code = 404, $message = null, $headers = [])
    {
        $code = (int) $code;

        if ($message === null) {
            $message = static::$statusTexts[$code] ?? 'Unknown Error';
        }

        if (php_sapi_name() === 'cli') {
            throw new Exception($message, $code);
        }

        http_response_code($code);

        foreach ($headers as $key => $value) {
            header($key . ': ' . $value);
        }

        // Try to find and render error view
        $viewPaths = [
            static::getViewsPath() . "/errors/{$code}.php",
            static::getViewsPath() . "/errors/default.php"
        ];

        foreach ($viewPaths as $viewPath) {
            if (file_exists($viewPath)) {
                // Pass variables to the view
                $error = [
                    'code' => $code,
                    'message' => $message
                ];

                // Extract variables for the view
                extract(compact('error'));

                // Start output buffering
                ob_start();
                include $viewPath;
                $content = ob_get_clean();

                echo $content;
                exit(1);
            }
        }

        // If no view is found, fallback to JSON response
        header('Content-Type: application/json');
        echo json_encode([
            'error' => [
                'code' => $code,
                'message' => $message
            ]
        ]);
        exit(1);
    }

    private function __construct() {}

    public static function redirect($url, $code = 302)
    {
        // Assicurati che gli header non siano già stati inviati
        if (!headers_sent()) {
            // Se l'URL non inizia con http o https, considera come URL relativo
            if (!preg_match('#^https?://#i', $url)) {
                // Usa BASE_URL se definita, altrimenti usa server name
                $baseUrl = Config::get('app.base_url', (
                    (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on'
                        ? "https"
                        : "http"
                    ) . "://" . $_SERVER['HTTP_HOST']));

                $url = rtrim($baseUrl, '/') . '/' . ltrim($url, '/');
            }

            header('Location: ' . $url, true, $code);
        }
        exit;
    }

    /**
     * Return JSON response
     *
     * @param  mixed  $data
     * @param  int  $status
     * @param  array  $headers
     * @return string
     */
    public static function json($data = [], $status = 200, array $headers = [])
    {
        http_response_code($status);

        foreach ($headers as $key => $value) {
            header($key . ': ' . $value);
        }

        header('Content-Type: application/json');

        echo json_encode($data);
        exit;
    }

    public static function jsonException($exception, $errorCode, $message, $debugInfo = "")
    {
        $response = [
            'exception' => $exception,
            'errorcode' => $errorCode,
            'message' => $message
        ];

        if (!empty($debugInfo)) {
            $response += ['debuginfo' => $debugInfo];
        }

        return self::json($response);
    }

    public static function notFound()
    {
        http_response_code(404);
    }

    public static function isJson($response)
    {
        json_decode($response);
        return json_last_error() === JSON_ERROR_NONE;
    }

    /**
     * Get the views base path
     * 
     * @return string
     */
    public static function getViewsPath()
    {
        $app = Application::app();
        $config = $app->config('filesystem');
        return rtrim($config['disks']['views'], '/');
    }
}
