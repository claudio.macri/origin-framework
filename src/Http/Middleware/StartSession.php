<?php

namespace Origin\Http\Middleware;

use Origin\Application;
use Origin\Http\Request;
use Origin\Http\Session;

class StartSession
{
    /**
     * Handle the request
     */
    public function handle(Request $request)
    {
        $session = Session::getInstance();

        // Configura le opzioni della sessione prima dello start
        $this->configureSession();

        if (!$session->isStarted()) {
            $session->start();
        }

        // Aggiungi la sessione alla request
        $request->setSession($session);
    }

    /**
     * Configure session options from config
     */
    protected function configureSession()
    {
        $config = Application::app()->config('session');

        if (headers_sent()) {
            return;
        }

        // Basic session settings
        ini_set('session.gc_maxlifetime', $config['gc_maxlifetime']);
        ini_set('session.cookie_lifetime', $config['lifetime']);
        ini_set('session.cache_expire', $config['cache_expire']);

        // Cookie settings
        session_set_cookie_params(
            $config['cookie']['lifetime'] ?? 0,
            $config['cookie']['path'] ?? '/',
            $config['cookie']['domain'] ?? null,
            $config['cookie']['secure'] ?? false,
            $config['cookie']['httponly'] ?? true
        );

        if (isset($config['cookie']['samesite'])) {
            session_set_cookie_params([
                'samesite' => $config['cookie']['samesite']
            ]);
        }
    }
}
