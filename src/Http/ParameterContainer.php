<?php

namespace Origin\Http;

class ParameterContainer
{
    protected $parameters;

    function __construct($parameters)
    {
        $this->parameters = $parameters;
    }

    function all()
    {
        return $this->parameters;
    }

    function keys()
    {
        return array_keys($this->parameters);
    }

    function replace($parameters)
    {
        $this->parameters = $parameters;
    }

    function add($parameters)
    {
        $this->parameters = array_replace($this->parameters, $parameters);
    }

    function get($key)
    {
        return $this->has($key) ? $this->parameters[$key] : null;
    }

    function set($key, $value)
    {
        $this->parameters[$key] = $value;
    }

    function has($key)
    {
        return array_key_exists($key, $this->parameters);
    }

    function remove($key)
    {
        if ($this->has($key)) {
            unset($this->parameters[$key]);
        }
    }

    function count()
    {
        return count($this->parameters);;
    }
}
