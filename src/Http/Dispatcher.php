<?php

namespace Origin\Http;

use Origin\Config;
use Origin\Routing\Router;

class Dispatcher
{
    protected $router;
    protected $middleware;
    protected $middlewareGroups;
    protected $middlewareAliases;

    public function __construct(array $middleware, array $middlewareGroups, array $middlewareAliases)
    {
        $this->router = Router::getInstance();
        $this->middleware = $middleware;
        $this->middlewareGroups = $middlewareGroups;
        $this->middlewareAliases = $middlewareAliases;
    }

    public function dispatch($request)
    {
        // Set middleware aliases nel router
        $this->router->setMiddleware($this->middlewareAliases);

        // Carica le routes con i rispettivi middleware groups
        $this->loadRoutes();

        // Esegui middleware globali
        foreach ($this->middleware as $middleware) {
            $response = $this->runMiddleware($middleware, $request);
            if ($response !== null) {
                return $response;
            }
        }

        // Dispatch alla route
        return $this->router->dispatch($request);
    }

    public function terminate($request, $response)
    {
        if ($response === false) {
            return Response::notFound();
        }

        if (Response::isJson($response)) {
            header('Content-Type: application/json');
        }

        echo $response;
    }

    protected function runMiddleware($middleware, $request)
    {
        return (new $middleware)->handle($request);
    }

    public function loadRoutes()
    {
        $this->router->group(['middleware' => $this->middlewareGroups['web']], function ($router) {
            $this->requireRouteFile(Config::get('filesystems.disks.root') . '/routes/web.php');
        });

        $this->router->group(['middleware' => $this->middlewareGroups['api'], 'prefix' => 'api'], function ($router) {
            $this->requireRouteFile(Config::get('filesystems.disks.root') . '/routes/api.php');
        });

        /* foreach ($this->router->getRoutes()->all() as $route) {
            echo $route->method . " " . ($route->name ?? $route->uri) . "<br>";
        }
        exit; */
    }

    protected function requireRouteFile($path)
    {
        if (file_exists($path)) {
            require $path;
        }
    }
}
