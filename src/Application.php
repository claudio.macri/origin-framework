<?php

namespace Origin;

class Application
{
    /**
     * The application instance.
     *
     * @var static
     */
    private static $instance;

    /**
     * The application configuration repository.
     *
     * @var Config
     */
    protected $config;

    /**
     * The service container instance.
     *
     * @var Container
     */
    protected $container;

    /**
     * Create a new application instance.
     *
     * @param string $configPath
     * @return void
     */
    public function __construct($configPath)
    {
        $this->container = new Container();
        static::$instance = $this;

        // Inizializza Config come singleton
        $this->config = new Config();
        $this->config->load($configPath, 'app');

        $this->container->instance('app', $this);
        $this->container->instance('config', $this->config);
    }

    /**
     * Get the application instance.
     *
     * @return static
     */
    public static function app()
    {
        return static::$instance;
    }

    /**
     * Get a configuration value.
     *
     * @param string $key
     * @param mixed $default
     * @return mixed
     */
    public function config($key = null, $default = null)
    {
        if (is_null($key)) {
            return $this->config;
        }
        return $this->config->get($key, $default);
    }

    /**
     * Load a configuration file.
     *
     * @param string $name
     * @param string $path
     * @return void
     */
    public function loadConfig($name, $path)
    {
        $this->config->load($path, $name);
    }

    /**
     * Make an instance of a class.
     *
     * @param string $abstract
     * @param array $parameters
     * @return mixed
     */
    public function make($abstract, array $parameters = [])
    {
        return $this->container->make($abstract, $parameters);
    }
}
