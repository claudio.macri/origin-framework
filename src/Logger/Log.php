<?php

namespace Origin\Logger;

use Origin\Application;
use Origin\Configuration;

class Log
{
    public const FATAL = "fatal";
    public const ERROR = "error";
    public const WARNING = "warning";
    public const INFO = "info";
    public const DEBUG = "debug";
    public const TRACE = "trace";

    private static ?Logger $instance = null;

    public static function setDefaultLogger(string $logPath, string $minLevel = self::ERROR): void
    {
        self::$instance = new Logger($logPath, $minLevel);
    }

    public static function fatal(string $message, ?array $context = null): void
    {
        self::ensureInstance();
        self::$instance->fatal($message, $context);
    }

    public static function error(string $message, ?array $context = null): void
    {
        self::ensureInstance();
        self::$instance->error($message, $context);
    }

    public static function warning(string $message, ?array $context = null): void
    {
        self::ensureInstance();
        self::$instance->warning($message, $context);
    }

    public static function info(string $message, ?array $context = null): void
    {
        self::ensureInstance();
        self::$instance->info($message, $context);
    }

    public static function debug(string $message, ?array $context = null): void
    {
        self::ensureInstance();
        self::$instance->debug($message, $context);
    }

    public static function trace(?string $message = null, ?array $context = null): void
    {
        self::ensureInstance();
        self::$instance->trace();
    }

    private static function ensureInstance(): void
    {
        if (self::$instance === null) {
            $app = Application::app();
            $config = $app->config('logging');

            if ($config === null) {
                throw new \RuntimeException("Default logger not set. Call Log::setDefaultLogger() first.");
            }

            self::setDefaultLogger($config['path'], $config['level']);
        }
    }
}
