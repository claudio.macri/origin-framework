<?php

namespace Origin\Logger;

use Exception;

class Logger
{
    protected const LEVELS = [
        self::FATAL => 0,
        self::ERROR => 1,
        self::WARNING => 2,
        self::INFO => 3,
        self::DEBUG => 4,
        self::TRACE => 5
    ];

    public const FATAL = "fatal";
    public const ERROR = "error";
    public const WARNING = "warning";
    public const INFO = "info";
    public const DEBUG = "debug";
    public const TRACE = "trace";

    protected string $logPath;
    protected string $minLevel;
    protected ?string $loggerName;

    // Costante per la lunghezza massima delle stringhe nel log
    private const MAX_STRING_LENGTH = 500;

    public function __construct(string $logPath, string $minLevel = self::ERROR, ?string $loggerName = null)
    {
        if (!is_writable($logPath) && !is_writable(dirname($logPath))) {
            throw new \RuntimeException("Cannot write to log file: $logPath");
        }
        $this->logPath = $logPath;
        $this->minLevel = $minLevel;
        $this->loggerName = $loggerName;
    }

    public function fatal(string $message, ?array $context = null): void
    {
        $this->log(self::FATAL, $message, $context);
    }

    public function error(string $message, ?array $context = null): void
    {
        $this->log(self::ERROR, $message, $context);
    }

    public function warning(string $message, ?array $context = null): void
    {
        $this->log(self::WARNING, $message, $context);
    }

    public function info(string $message, ?array $context = null): void
    {
        $this->log(self::INFO, $message, $context);
    }

    public function debug(string $message, ?array $context = null): void
    {
        $this->log(self::DEBUG, $message, $context);
    }

    public function trace(?string $message = null, ?array $context = null): void
    {
        // Otteniamo l'intero stack trace
        $trace = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS);

        // Troviamo la chiamata al trace effettiva (dopo le classi Logger)
        $logCall = null;
        foreach ($trace as $t) {
            if (
                !isset($t['class']) ||
                ($t['class'] !== Logger::class &&
                    $t['class'] !== Log::class)
            ) {
                $logCall = $t;
                break;
            }
        }

        if (!$logCall) {
            return;
        }

        if ($message === null) {
            $message = "Richiamato metodo";

            if (isset($logCall['function'])) {
                $message .= " " . $logCall['function'];
            }

            if (isset($logCall['class'])) {
                $message .= " della classe " . $logCall['class'];
            }
        }

        $this->log(self::TRACE, $message, $context);
    }

    protected function log(string $level, string $message, ?array $context = null): void
    {
        if (self::LEVELS[$level] > self::LEVELS[$this->minLevel]) {
            return;
        }

        $trace = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS);

        // Troviamo prima la chiamata al log effettiva (dopo le classi Logger)
        $logCall = null;
        foreach ($trace as $i => $t) {
            if (
                !isset($t['class']) ||
                ($t['class'] !== Logger::class &&
                    $t['class'] !== Log::class)
            ) {
                $logCall = $t;
                break;
            }
        }

        if ($logCall) {
            // Creiamo il caller solo con le informazioni disponibili
            $caller = [
                'class' => $logCall['class'] ?? null,
                'function' => $logCall['function'] ?? null
            ];

            // Aggiungiamo file e line solo se disponibili
            if (isset($logCall['file'])) {
                $caller['file'] = $logCall['file'];
            }
            if (isset($logCall['line'])) {
                $caller['line'] = $logCall['line'];
            }
        }

        $logEntry = $this->formatLogEntry($level, $message, $context, $caller);
        file_put_contents($this->logPath, $logEntry, FILE_APPEND | LOCK_EX);
    }

    protected function formatLogEntry(string $level, string $message, ?array $context, ?array $trace): string
    {
        $timestamp = date('Y-m-d H:i:s');
        $caller = $this->formatCaller($trace);

        $entry = "[$timestamp][" . strtoupper($level) . "]";

        if ($this->loggerName) {
            $entry .= "[{$this->loggerName}]";
        }

        if ($caller) {
            $entry .= "[$caller]";
        }

        $entry .= " $message";

        if ($context) {
            $sanitizedContext = $this->sanitizeContext($context);
            $entry .= " " . json_encode($sanitizedContext, JSON_UNESCAPED_UNICODE);
        }

        return $entry . PHP_EOL;
    }

    protected function sanitizeContext(array $context): array
    {
        $sanitized = [];
        foreach ($context as $key => $value) {
            if (is_object($value)) {
                if (method_exists($value, '__toString')) {
                    $sanitized[$key] = $this->truncateString((string)$value);
                } else {
                    $sanitized[$key] = get_class($value);
                }
            } elseif (is_array($value)) {
                $sanitized[$key] = $this->sanitizeContext($value);
            } elseif ($value instanceof \Closure) {
                $sanitized[$key] = '[closure]';
            } elseif (is_resource($value)) {
                $sanitized[$key] = '[resource]';
            } elseif (is_string($value)) {
                $sanitized[$key] = $this->truncateString($value);
            } else {
                $sanitized[$key] = $value;
            }
        }
        return $sanitized;
    }

    protected function formatCaller(?array $trace): string
    {
        if (!$trace) {
            return '';
        }

        $caller = '';
        if (isset($trace['class'])) {
            $caller .= $trace['class'] . '::';
        }
        if (isset($trace['function'])) {
            $caller .= $trace['function'];
        }
        if (isset($trace['file'], $trace['line'])) {
            $caller .= ' (' . basename($trace['file']) . ':' . $trace['line'] . ')';
        }

        return $caller;
    }

    private function truncateString(string $string): string
    {
        if (strlen($string) <= self::MAX_STRING_LENGTH) {
            return $string;
        }

        // Se sembra HTML tronchiamo se supera la lunghezza massima
        if ($this->looksLikeHtml($string) && strlen($string) > self::MAX_STRING_LENGTH) {
            return substr($string, 0, self::MAX_STRING_LENGTH) . '... [HTML truncated]';
        }

        return $string;
    }

    private function looksLikeHtml(string $string): bool
    {
        return strpos($string, '<') !== false &&
            strpos($string, '>') !== false &&
            (
                stripos($string, '</') !== false ||
                stripos($string, '/>') !== false
            );
    }
}
